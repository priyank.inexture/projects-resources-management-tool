/* eslint-disable no-nested-ternary */
import React, { createContext, useReducer, useContext } from 'react';
import { notification } from 'antd';
// eslint-disable-next-line max-len
import { INIT_APP,TEST_ACTION_TYPE,UPDATE_PROJECTS,UPDATE_NOTIFICATION,UPDATE_LOADING,UPDATE_CURRENCY,UPDATE_CURRENCY_TYPE,UPDATE_RESOURCES,UPDATE_CLIENTS,UPDATE_HEADER_TITLE,UPDATE_AVAILABLE_TYPE,UPDATE_AUTHENTICATION,UPDATE_MENU_SELECTED_KEY } from '../constants/ActionType';
//Define Conext
const GlobalStateContext = createContext();
const GlobalDispatchContext = createContext();

//Reducer
const globalReducer = (state, action) => {
    switch (action.type) {
        case TEST_ACTION_TYPE: {
            return {
                ...state,
                projects: action.projects
            };
        }
        case UPDATE_LOADING: {
            return {
                ...state,
                isLoading: action.loading
            };
        }
        case UPDATE_PROJECTS: {
            return {
                ...state,
                projects: action.projects
            };
        }
        case UPDATE_RESOURCES: {
            return {
                ...state,
                employees: action.employees
            };
        }
        case UPDATE_CLIENTS: {
            return {
                ...state,
                clients: action.clients
            };
        }
        case UPDATE_NOTIFICATION: {
            (() => {
                notification[action.notification.type]({
                    message: action.notification.title ? action.notification.title : '',
                    description: action.notification.msg ? action.notification.msg : ''
                });
            })();
            return {
                ...state,
                notification: {
                    type: action.notification.type ? action.notification.type : 'info',
                    title: action.notification.title ? action.notification.title : '',
                    description: action.notification.msg ? action.notification.msg : ''
                }
            };
        }
        case INIT_APP: {
            return {
                ...state,
                isLoading: false,
                projects: action.load.projects,
                clients: action.load.clients,
                employees: action.load.employees,
                availableType: action.load.availableType
            };
        }
        case UPDATE_CURRENCY: {
            let rates = action.currency ? (action.currency.rates ? action.currency.rates : { USD: 1.0 }) : { USD: 1.0 };
            return {
                ...state,
                currency: {
                    from: 'USD',
                    to: 'USD',
                    fromRate: 1,
                    toRate: 1,
                    rates: rates,
                    options: Object.keys(rates)
                }
            };
        }
        case UPDATE_CURRENCY_TYPE: {
            return {
                ...state,
                currency: {
                    ...state.currency,
                    to: action.to ? action.to : 'USD',
                    toRate: state.currency.rates[action.to] ? state.currency.rates[action.to] : 1
                }
            };
        }
        case UPDATE_HEADER_TITLE: {
            return {
                ...state,
                headerTitle: action.headerTitle ? action.headerTitle : ''
            };
        }
        case UPDATE_AVAILABLE_TYPE: {
            return {
                ...state,
                availableType: {
                    payment: (action.typeOf === 'payment') ? action.load : state.availableType.payment,
                    allocation: (action.typeOf === 'allocation') ? action.load : state.availableType.allocation,
                    priority: (action.typeOf === 'priority') ? action.load : state.availableType.priority,
                    status: (action.typeOf === 'status') ? action.load : state.availableType.status,
                    role: (action.typeOf === 'role') ? action.load : state.availableType.role,
                    designation: (action.typeOf === 'designation') ? action.load : state.availableType.designation,
                    technology: (action.typeOf === 'technology') ? action.load : state.availableType.technology
                }
            };
        }
        case UPDATE_AUTHENTICATION: {
            return {
                ...state,
                isAuthenticated: action.load,
                isSuperUser: action.isSuperUser ? true : false
            };
        }
        case UPDATE_MENU_SELECTED_KEY: {
            return {
                ...state,
                menuSelectedKey: action.menuSelectedKey
            };
        }
        default: {
            throw new Error(`Unhandled action type: ${action.type}`);
        }
    }
};

//Provider

// eslint-disable-next-line react/prop-types
export const GlobalProvider = ({ children }) => {
    const [state, dispatch] = useReducer(globalReducer, {
        isAuthenticated: localStorage.getItem('token') ? true : false,
        isSuperUser: localStorage.getItem('isSuperUser') === 'true' ? true : false,
        // isAuthenticated: false,
        // isSuperUser: false,
        projects: [],
        clients: [],
        employees: [],
        isLoading: true,
        availableType: {
            payment: [],
            allocation: [],
            priority: [],
            status: [],
            role: [],
            designation: [],
            technology: []
        },
        notification: {
            type: 'info',
            title: '',
            description: ''
        },
        currency: {
            from: 'USD',
            to: 'USD',
            rates: {
                USD: 1
            },
            fromRate: 1,
            toRate: 1,
            options: ['USD']
        },
        headerTitle: '',
        menuSelectedKey: '1'
    });

    return (
        <GlobalDispatchContext.Provider value={dispatch}>
            <GlobalStateContext.Provider value={state}>
                {children}
            </GlobalStateContext.Provider>
        </GlobalDispatchContext.Provider>
    );
};

//custom hooks for when we want to use our global state
export const useGlobalStateContext = () => useContext(GlobalStateContext);

export const useGlobalDispatchContext = () => useContext(GlobalDispatchContext);
