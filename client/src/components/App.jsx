import React, { useEffect } from 'react';
import { hot } from 'react-hot-loader/root';
import { Menu, Layout, Spin, Row, Col, Select, Button } from 'antd';
import { TableOutlined, ProjectOutlined, UserOutlined, TeamOutlined } from '@ant-design/icons';
import { BrowserRouter as Router, Switch, Route, Link, Redirect } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import { fetchTestData } from '../apiActions/TestAction';
import Master from '../pages/Master';
import Projects from '../pages/Projects';
import ProjectDetail from '../pages/ProjectDetail';
import Resources from '../pages/Resources';
import Clients from '../pages/Clients';
import PrivateRoute from './PrivateRoute';
import { useGlobalStateContext, useGlobalDispatchContext } from '../context/globalContext';
import useCurrency from '../hooks/useCurrency';
import { INIT_APP, UPDATE_CURRENCY_TYPE, UPDATE_LOADING, UPDATE_AUTHENTICATION, UPDATE_NOTIFICATION, UPDATE_MENU_SELECTED_KEY } from '../constants/ActionType';
import { SESSION_TIME } from '../constants/ApiConstants';
import ResourceDetail from '../pages/ResourceDetail';
import Login from '../pages/Login';
import ClientDetail from '../pages/ClientDetail';
import Logo from '../assets/images/logo.png';
const { Header, Content, Footer, Sider } = Layout;
const { Option } = Select;
const menuItemList = [
    { icon: TableOutlined, to: '/master', text: 'Master', forSuperUser: true },
    { icon: ProjectOutlined, to: '/projects', text: 'Projects', forSuperUser: false },
    { icon: UserOutlined, to: '/resources', text: 'Resources', forSuperUser: true },
    { icon: TeamOutlined, to: '/clients', text: 'Clients', forSuperUser: true }
];
function App() {
    const dispatch = useGlobalDispatchContext();
    const { isLoading, currency, headerTitle, isAuthenticated, isSuperUser, menuSelectedKey } = useGlobalStateContext();
    const usedRates = useCurrency();
    const onChange = (value) => {
        dispatch({ type: UPDATE_CURRENCY_TYPE, to: value });
    };
    // eslint-disable-next-line space-before-function-paren
    useEffect(async () => {
        console.log('Using rates of ', usedRates.date);
        if (isAuthenticated) {
            dispatch({ type: UPDATE_LOADING, loading: true });
            let response = await fetchTestData();
            dispatch({ type: INIT_APP, load: response.data });
            setTimeout(() => {
                if (isAuthenticated) {
                    if (localStorage.getItem('token')) {
                        dispatch({
                            type: UPDATE_NOTIFICATION, notification: {
                                type: 'info',
                                title: 'Session Time over!'
                            }
                        });
                        localStorage.removeItem('token');
                        localStorage.removeItem('isSuperUser');
                    }
                    dispatch({ type: UPDATE_AUTHENTICATION, load: false, isSuperUser: false });
                }
            }, SESSION_TIME);
        } else {
            localStorage.removeItem('token');
            localStorage.removeItem('isSuperUser');
            dispatch({ type: UPDATE_LOADING, loading: false });
        }
        // dispatch({ type: UPDATE_RATES, currency: currency });
    }, [isAuthenticated]);
    const onLogout = () => {
        localStorage.removeItem('token');
        localStorage.removeItem('isSuperUser');
        dispatch({ type: UPDATE_AUTHENTICATION, load: false, isSuperUser: false });
    };
    return (
        <>
            <Helmet>
                <title>Projects & Resources Management Tool</title>
            </Helmet>
            <Router>
                {isAuthenticated &&
                    <Spin tip="Loading..." spinning={isLoading}>
                        <Layout style={{ minHeight: '100vh' }}>
                            <Sider
                                breakpoint="lg"
                                collapsedWidth="0"
                            >
                                <div style={{
                                    height: 32,
                                    margin: 16,
                                    background: 'rgba(255, 255, 255, 0.2)'
                                }} >
                                    <img src={Logo} style={{ width: '100%' }} />
                                </div>
                                <Helmet>
                                    <title>{headerTitle}</title>
                                </Helmet>
                                <Menu onClick={(e) => dispatch({ type: UPDATE_MENU_SELECTED_KEY, menuSelectedKey: e.key })} theme="dark" mode="inline" defaultSelectedKeys={['1']} selectedKeys={[menuSelectedKey]}>
                                    {
                                        menuItemList.map((m, index) => {
                                            if (m.forSuperUser && isSuperUser) {
                                                return (<Menu.Item key={index + 1} icon={<m.icon />}>
                                                    <Link to={m.to}>{m.text}</Link>
                                                </Menu.Item>);
                                            } else if (!m.forSuperUser) {
                                                return (<Menu.Item key={index + 1} icon={<m.icon />}>
                                                    <Link to={m.to}>{m.text}</Link>
                                                </Menu.Item>);
                                            }
                                            return null;
                                        })
                                    }
                                </Menu>
                            </Sider>
                            <Layout>
                                <Header style={{ padding: 0, background: '#fff' }} >
                                    <Row justify="space-between">
                                        <Col style={{ marginLeft: '16px' }}>
                                            <h1>{headerTitle}</h1>
                                        </Col>
                                        {
                                            isAuthenticated &&
                                            <Col span={4}>
                                                <Select
                                                    showSearch
                                                    style={{ width: 75 }}
                                                    placeholder="Currency"
                                                    optionFilterProp="children"
                                                    defaultValue="USD"
                                                    onChange={onChange}
                                                    // onSearch={onSearch}
                                                    // options={currency.options}
                                                    filterOption={(input, option) =>
                                                        // console.log(option)
                                                        option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                                                    }
                                                >
                                                    {currency.options.length === 0 && <Option key="USD" value="USD">USD</Option>}
                                                    {currency.options.map(o => <Option key={o} value={o}>{o}</Option>)}
                                                </Select>
                                                <Button style={{ marginLeft: 15 }} type='danger' onClick={onLogout}>Logout</Button>
                                            </Col>
                                        }
                                    </Row>
                                </Header>
                                <Content style={{ margin: '24px 16px 0' }}>
                                    <Switch>
                                        <PrivateRoute path="/projects/:id" Component={ProjectDetail} />
                                        <PrivateRoute path="/projects" Component={Projects} />
                                        <PrivateRoute path="/resources/:id" Component={ResourceDetail} />
                                        <PrivateRoute path="/resources" Component={Resources} />
                                        <PrivateRoute path="/clients/:id" Component={ClientDetail} />
                                        <PrivateRoute path="/clients" Component={Clients} />
                                        <PrivateRoute path={['/master', '/']} Component={Master} />
                                    </Switch>
                                </Content>
                                <Footer style={{ textAlign: 'center' }}>Projects-resources-management-tool</Footer>
                            </Layout>
                        </Layout>
                    </Spin >
                }
                {!isAuthenticated &&
                    <Redirect to='/login' />
                }
                <Switch>
                    <Route path={['/login']}>
                        <Helmet>
                            <title>Login</title>
                        </Helmet>
                        <Login />
                    </Route>
                </Switch>
            </Router>
        </>);
}
export default hot(App);
