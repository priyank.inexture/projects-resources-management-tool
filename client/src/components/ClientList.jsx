/* eslint-disable react/prop-types */
import React, { useState, useEffect } from 'react';
import { AutoComplete, Button, Row, Col, Table, Popconfirm } from 'antd';
import { DeleteOutlined } from '@ant-design/icons';
import { useParams, Link } from 'react-router-dom';
import { addClientToProject, removeClientFromProject } from '../apiActions/ProjectAction';
import { useGlobalDispatchContext, useGlobalStateContext } from '../context/globalContext';
import { UPDATE_NOTIFICATION, UPDATE_PROJECTS } from '../constants/ActionType';
function ClientList({ list }) {
    let { id } = useParams();
    const { clients, isSuperUser } = useGlobalStateContext();
    const dispatch = useGlobalDispatchContext();
    const [dataClients, setDataClients] = useState([]);
    const [selectedResource, setSelectedResource] = useState({ key: null, value: null });
    const [options, setOptions] = useState([]);
    const [value, setValue] = useState('');
    useEffect(() => {
        let data = clients.map((c) => ({ key: c._id, value: `${c.firstName} ${c.lastName}` }));
        setOptions(data);
        setDataClients(data);
    }, [clients]);
    const superColumns = [
        {
            title: 'Action',
            width: 100,
            // eslint-disable-next-line react/display-name
            render: (record) => (
                <Popconfirm
                    title="Are you sure to delete this client from project?"
                    onConfirm={() => deleteClient(record)}
                    okText="Confirm"
                    cancelText="Cancel"
                >
                    <DeleteOutlined style={{ color: '#f81d22' }} />
                </Popconfirm >
            )
        }
    ];
    const columns = [
        {
            title: 'Id',
            dataIndex: '_id',
            // eslint-disable-next-line react/display-name
            render: (text) => (<Link to={`/clients/${text}`}>{text}</Link>)
        },
        {
            title: 'First Name',
            dataIndex: 'firstName',
            defaultSortOrder: 'ascend',
            sorter: (a, b) => a.firstName.localeCompare(b.firstName)
        },
        {
            title: 'Last  Name',
            dataIndex: 'lastName',
            sorter: (a, b) => a.lastName.localeCompare(b.lastName)
        },
        {
            title: 'Email',
            dataIndex: 'email',
            // eslint-disable-next-line react/display-name
            render: (text) => (<a href={`mailto:${text}`}>{text}</a>)
        },
        {
            title: 'Contact Number',
            dataIndex: 'contactNumber',
            // eslint-disable-next-line react/display-name
            render: (text) => (<a href={`tel:${text}`}>{text}</a>)
        },
        {
            title: 'Address',
            dataIndex: 'address'
        },
        ...(isSuperUser ? superColumns : [])
    ];
    const onSearch = (val) => {
        let filtered = dataClients.filter(
            (obj) => obj.key !== 0 && obj.value.toString().toLowerCase().includes(val)
        );
        setOptions(filtered);
        setValue(val);
    };
    const onSelect = (val, option) => {
        setValue(val);
        setSelectedResource(option);
    };
    const deleteClient = (record) => {
        console.log('deleting', id, record._id);
        removeClientFromProject(id, record._id).then(res => {
            dispatch({
                type: UPDATE_NOTIFICATION, notification: {
                    type: 'success',
                    title: `Successfully removed ${record.firstName} ${record.lastName}`
                }
            });
            dispatch({ type: UPDATE_PROJECTS, projects: res.data.projects });
        }).catch(error => {
            if (error.response) {
                if (error.response.data.error) {
                    dispatch({
                        type: UPDATE_NOTIFICATION, notification: {
                            type: 'info',
                            title: error.response.data.error
                        }
                    });
                }
            }
        });
    };
    const handleAdd = () => {
        if (selectedResource.key) {
            addClientToProject(id, selectedResource.key).then(res => {
                dispatch({ type: UPDATE_PROJECTS, projects: res.data.projects });
                dispatch({
                    type: UPDATE_NOTIFICATION, notification: {
                        type: 'success',
                        title: `Successfully added ${selectedResource.value}`
                    }
                });
            }).catch(error => {
                if (error.response) {
                    if (error.response.data.error) {
                        dispatch({
                            type: UPDATE_NOTIFICATION, notification: {
                                type: 'info',
                                title: error.response.data.error
                            }
                        });
                    } else {
                        console.log(error.response);
                    }
                } else {
                    // console.log('some error occ');
                    //no user exist with such id
                    // dispatch({
                    //     type: UPDATE_NOTIFICATION, notification: {
                    //         type: 'danger',
                    //         title: 'No such user'
                    //     }
                    // });
                }
            }).finally(() => {
                setSelectedResource({ key: null, value: null });
                setOptions([]);
                setValue('');
            });
        } else {
            dispatch({
                type: UPDATE_NOTIFICATION, notification: {
                    type: 'warning',
                    title: 'No user selected'
                }
            });
            setSelectedResource({ key: null, value: null });
            setOptions([]);
            setValue('');
        }
    };
    return (
        <>
            {
                isSuperUser &&
                <Row style={{ marginBottom: 25 }}>
                    <Col>
                        <AutoComplete
                            style={{ width: 400 }}
                            options={options}
                            placeholder="Add Client"
                            value={value} searchValue={value}
                            onSelect={(val, option) => onSelect(val, option)}
                            onSearch={onSearch}
                        />
                        <Button type="primary" onClick={handleAdd}>
                            Add
                        </Button>
                    </Col>
                </Row>
            }
            <Table columns={columns} dataSource={list} rowKey="_id" />
        </>
    );
}

export default ClientList;
