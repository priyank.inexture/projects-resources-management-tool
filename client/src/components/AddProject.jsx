/* eslint-disable max-nested-callbacks */
import React, { useMemo } from 'react';
import {
    Form,
    Input,
    Button,
    AutoComplete,
    Space,
    Select,
    InputNumber,
    Tooltip
} from 'antd';
import { MinusCircleOutlined, PlusOutlined } from '@ant-design/icons';
const { Option } = Select;
import { useGlobalStateContext, useGlobalDispatchContext } from '../context/globalContext';
import { postProjectData } from '../apiActions/ProjectAction';
import { UPDATE_PROJECTS, UPDATE_NOTIFICATION } from '../constants/ActionType';
// eslint-disable-next-line react/prop-types
function AddProject({ setAdd }) {
    const dispatch = useGlobalDispatchContext();
    const { availableType, employees } = useGlobalStateContext();
    const formItemLayout = {
        labelCol: {
            xs: {
                span: 24
            },
            sm: {
                span: 8
            }
        },
        wrapperCol: {
            xs: {
                span: 24
            },
            sm: {
                span: 16
            }
        }
    };
    const tailFormItemLayout = {
        wrapperCol: {
            xs: {
                span: 24,
                offset: 0
            },
            sm: {
                span: 16,
                offset: 8
            }
        }
    };
    const [form] = Form.useForm();
    // const [autoCompleteResult, setAutoCompleteResult] = useState([]);
    const formItemLayoutWithOutLabel = {
        wrapperCol: {
            xs: { span: 24, offset: 0 },
            sm: { span: 20, offset: 4 }
        }
    };
    const onFinish = (values) => {
        values.technologies = [...new Set(values.technologies)];
        // console.log('Received values of form: ', values);
        postProjectData(values).then(res => {
            dispatch({ type: UPDATE_PROJECTS, projects: res.data.projects });
            dispatch({
                type: UPDATE_NOTIFICATION, notification: {
                    type: 'success',
                    title: 'Added Project successfully'
                }
            });
        }).catch(error => {
            if (error.response) {
                dispatch({
                    type: UPDATE_NOTIFICATION, notification: {
                        type: 'error',
                        title: error.response.data.error ? error.response.data.error : 'Some error Occured, cannot perform action!'
                    }
                });
            }
        }).finally(() => setAdd(false));
        form.resetFields();
    };
    // const employeesList = useMemo(() => {
    //     return employees.map((e) => ({ value: e.userName, label: e.userName }));
    // }, [employees]);
    const technologiesList = useMemo(() => {
        return availableType.technology.map((t) => (t.status ? { value: t.value, label: t.value } : null)).filter(t => t);
    }, [availableType]);
    const options = useMemo(() => {
        let opt = availableType.designation.map(d => ({ label: d.value, options: employees.filter(e => e.designation === d.value).map(emp => ({ value: emp.userName, label: `${emp.firstName} ${emp.lastName}` })) }));
        return opt.filter(o => o.options.length !== 0);
        // return employees.map((e) => ({ value: e.userName, label: e.userName }));
    }, [employees, availableType]);
    return (
        <Form
            {...formItemLayout}
            form={form}
            name="register"
            onFinish={onFinish}
            initialValues={{
                amount: 0
            }}
            scrollToFirstError
        >
            <Form.Item
                name="name"
                label={
                    <span>
                        Project Name
                    </span>
                }
                rules={[
                    {
                        required: true,
                        message: 'Please input your Project Name!',
                        whitespace: true
                    }
                ]}
            >
                <Input />
            </Form.Item>
            <Form.Item name="paymentType" label="Payment Type" rules={[{ required: true }]}>
                <Select
                    placeholder="Payment Type"
                >
                    {
                        availableType.payment
                            .map((pt, index) => pt.status ? <Option key={index} value={pt.value}>
                                <Tooltip title={pt.description}>
                                    {pt.value}
                                </Tooltip>
                            </Option> : null)
                    }
                </Select>
            </Form.Item>
            <Form.Item name="amount" label="Total Amount (in USD)" rules={[{ required: true }]}>
                <InputNumber
                    min={0}
                    style={{ width: 150 }}
                    max={100000000}
                // formatter={value => currencyFormat(value, currency.to, currency.toRate)}
                // parser={value => value.replace(/₹\s?|(,*)/g, '')}
                // onChange={onChange}
                />
            </Form.Item>
            <Form.Item name="allocation" label="Allocation" rules={[{ required: true }]}>
                <Select
                    placeholder="Select Allocation Type"
                >
                    {
                        availableType.allocation
                            .map((at, index) => at.status ? <Option key={index} value={at.value}>
                                <Tooltip title={at.description}>
                                    {at.value}
                                </Tooltip>
                            </Option> : null)
                    }
                </Select>
            </Form.Item>
            <Form.Item name="priority" label="Priority" rules={[{ required: true }]}>
                <Select
                    placeholder="Select Priority"
                >
                    {
                        availableType.priority
                            .map((pt, index) => pt.status ? <Option key={index} value={pt.value}>
                                <Tooltip title={pt.description}>
                                    {pt.value}
                                </Tooltip>
                            </Option> : null)
                    }
                </Select>
            </Form.Item>
            <Form.Item name="status" label="Status" rules={[{ required: true }]}>
                <Select
                    placeholder="Select status"
                >
                    {
                        availableType.status
                            .map((st, index) => st.status ? <Option key={index} value={st.value}>
                                <Tooltip title={st.description}>
                                    {st.value}
                                </Tooltip>
                            </Option> : null)
                    }
                </Select>
            </Form.Item>
            <Form.Item name="teamLead" label="Team Lead" rules={[{ required: false }]}>
                <AutoComplete
                    style={{ width: 200 }}
                    options={options}
                    placeholder="Select Team Lead"
                    filterOption={(inputValue, option) => {
                        return option.label.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1;
                    }}
                />
            </Form.Item>
            <Form.Item name="reviewer" label="Reviewer" rules={[{ required: false }]}>
                <AutoComplete
                    style={{ width: 200 }}
                    options={options}
                    placeholder="Select Reviewer"
                    filterOption={(inputValue, option) => {
                        return option.label.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1;
                    }}
                />
            </Form.Item>
            <Form.Item label="Technologies">
                <Form.List name="technologies" {...formItemLayoutWithOutLabel}>
                    {(fields, { add, remove }) => (
                        <>
                            {fields.map(field => (
                                <Space key={field.key} style={{ display: 'flex', marginBottom: 8 }} align="baseline">
                                    <Form.Item
                                        {...field}
                                        rules={[{ required: true, message: 'This is required Field' }]}
                                    >
                                        <AutoComplete
                                            style={{ width: 200 }}
                                            options={technologiesList}
                                            placeholder="Select Technology"
                                            filterOption={(inputValue, option) => {
                                                return option.label.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1;
                                            }}
                                        />
                                    </Form.Item>
                                    <MinusCircleOutlined onClick={() => remove(field.name)} />
                                </Space>
                            ))}
                            <Form.Item>
                                <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />}>
                                    Add Technologies
                                </Button>
                            </Form.Item>
                        </>
                    )}
                </Form.List>
            </Form.Item>
            <Form.Item {...tailFormItemLayout}>
                <Button type="primary" htmlType="submit">
                    Register
                </Button>
            </Form.Item>
        </Form>
    );
}

export default AddProject;
