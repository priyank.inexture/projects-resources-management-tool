import React, { useMemo } from 'react';
import {
    Form,
    Button,
    AutoComplete,
    Select,
    Tooltip
} from 'antd';
const { Option } = Select;
import { useGlobalStateContext, useGlobalDispatchContext } from '../context/globalContext';
import { postResponsibilityData } from '../apiActions/ResponsibilityAction';
import { UPDATE_NOTIFICATION } from '../constants/ActionType';
// eslint-disable-next-line react/prop-types
function AddResourceToProject({ setAdd, project, setProject }) {
    const dispatch = useGlobalDispatchContext();
    const { availableType, employees } = useGlobalStateContext();
    const formItemLayout = {
        labelCol: {
            xs: {
                span: 24
            },
            sm: {
                span: 4
            }
        },
        wrapperCol: {
            xs: {
                span: 24
            },
            sm: {
                span: 16
            }
        }
    };
    const tailFormItemLayout = {
        wrapperCol: {
            xs: {
                span: 24,
                offset: 0
            },
            sm: {
                span: 16,
                offset: 4
            }
        }
    };
    const [form] = Form.useForm();
    // const [autoCompleteResult, setAutoCompleteResult] = useState([]);
    // const formItemLayoutWithOutLabel = {
    //     wrapperCol: {
    //         xs: { span: 24, offset: 0 },
    //         sm: { span: 20, offset: 4 }
    //     }
    // };
    const onFinish = (values) => {
        // values.technologies = [...new Set(values.technologies)];
        values.project = project;
        console.log('Received values of form: ', values);
        postResponsibilityData(values).then(res => {
            if (res.data.responsibilities) {
                setProject((prevProject) => ({ ...prevProject, responsibilities: res.data.responsibilities }));
                dispatch({
                    type: UPDATE_NOTIFICATION, notification: {
                        type: 'success',
                        title: 'Added Resource to Project successfully'
                    }
                });
            }
        }).catch(error => {
            if (error.response) {
                dispatch({
                    type: UPDATE_NOTIFICATION, notification: {
                        type: 'error',
                        title: error.response.data.error ? error.response.data.error : 'Some error Occured, cannot perform action!'
                    }
                });
            }
        }).finally(() => setAdd(false));
        form.resetFields();
    };
    const options = useMemo(() => {
        // return employees.map((e) => ({ value: e.userName, label: e.userName }));
        // eslint-disable-next-line max-nested-callbacks
        // console.log(employees.filter(e=>e.designation);
        // eslint-disable-next-line max-nested-callbacks
        // console.log();
        // eslint-disable-next-line max-nested-callbacks
        let opt = availableType.designation.map(d => ({ label: d.value, options: employees.filter(e => e.designation === d.value).map(emp => ({ value: emp.userName, label: `${emp.firstName} ${emp.lastName}` })) }));
        return opt.filter(o => o.options.length !== 0);
        // return employees.map((e) => ({ value: e.userName, label: e.userName }));
    }, [employees, availableType]);
    // const technologiesList = useMemo(() => {
    //     return availableType.technology.map((t) => ({ value: t.value, label: t.value }));
    // }, [availableType]);
    const notList = ['Team Lead', 'Reviewer'];
    // const notList = [];
    return (
        <Form
            {...formItemLayout}
            form={form}
            name="register"
            onFinish={onFinish}
            initialValues={{
                amount: 0
            }}
            scrollToFirstError
        >
            <Form.Item name="employee" label="Resource" rules={[{ required: false }]}>
                <AutoComplete
                    style={{ width: 200 }}
                    options={options}
                    placeholder="Select Resource"
                    filterOption={(inputValue, option) => {
                        return option.label.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1;
                    }}
                />
            </Form.Item>
            <Form.Item name="role" label="Role" rules={[{ required: true }]}>
                <Select
                    placeholder="Select role"
                >
                    {
                        availableType.role.filter(r => !notList.includes(r.value))
                            .map((rt, index) => rt.status ? <Option key={index} value={rt.value}>
                                <Tooltip title={rt.description}>
                                    {rt.value}
                                </Tooltip>
                            </Option> : null)
                    }
                </Select>
            </Form.Item>
            {/* <Form.Item label="Technologies">
                <Form.List name="technologies" {...formItemLayoutWithOutLabel}>
                    {(fields, { add, remove }) => (
                        <>
                            {fields.map(field => (
                                <Space key={field.key} style={{ display: 'flex', marginBottom: 8 }} align="baseline">
                                    <Form.Item
                                        {...field}
                                        rules={[{ required: true, message: 'This is required Field' }]}
                                    >
                                        <AutoComplete
                                            style={{ width: 200 }}
                                            options={technologiesList}
                                            placeholder="Select Technology"
                                            filterOption={(inputValue, option) => {
                                                return option.label.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1;
                                            }}
                                        />
                                    </Form.Item>
                                    <MinusCircleOutlined onClick={() => remove(field.name)} />
                                </Space>
                            ))}
                            <Form.Item>
                                <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />}>
                                    Add Technologies
                                </Button>
                            </Form.Item>
                        </>
                    )}
                </Form.List>
            </Form.Item> */}
            <Form.Item {...tailFormItemLayout}>
                <Button type="primary" htmlType="submit">
                    Add Resource
                </Button>
            </Form.Item>
        </Form>
    );
}

export default AddResourceToProject;
