/* eslint-disable react/prop-types */
import React from 'react';
import { Form, Select, Button, Tooltip, Input } from 'antd';
import { useGlobalDispatchContext, useGlobalStateContext } from '../context/globalContext';
import { UPDATE_NOTIFICATION } from '../constants/ActionType';
import { putResponsibilityData } from '../apiActions/ResponsibilityAction';
const { Option } = Select;
function EditResourceInProject({ resource, onDiscard, setProject, responsibilities }) {
    const { availableType } = useGlobalStateContext();
    const dispatch = useGlobalDispatchContext();
    const formItemLayout = {
        labelCol: {
            xs: {
                span: 24
            },
            sm: {
                span: 8
            }
        },
        wrapperCol: {
            xs: {
                span: 24
            },
            sm: {
                span: 16
            }
        }
    };
    const tailFormItemLayout = {
        wrapperCol: {
            xs: {
                span: 24,
                offset: 0
            },
            sm: {
                span: 16,
                offset: 8
            }
        }
    };
    const [form] = Form.useForm();
    // const formItemLayoutWithOutLabel = {
    //     wrapperCol: {
    //         xs: { span: 24, offset: 0 },
    //         sm: { span: 20, offset: 4 }
    //     }
    // };
    const onFinish = (values) => {
        // values.technologies = [...new Set(values.technologies)];
        // console.log('Updated ', values);
        console.log('Received values of form: ', values);
        putResponsibilityData(values, resource._id).then(res => {
            if (res.data.responsibility) {
                let newresponsibilities = responsibilities.map(r => ((r._id === resource._id) ? { ...res.data.responsibility } : r));
                setProject(prevProject => ({ ...prevProject, responsibilities: newresponsibilities }));
                // dispatch({ type: UPDATE_PROJECTS, projects: res.data.projects });
                dispatch({
                    type: UPDATE_NOTIFICATION, notification: {
                        type: 'success',
                        title: 'Updated resource successfully'
                    }
                });
            }
        }).catch(error => {
            if (error.response) {
                dispatch({
                    type: UPDATE_NOTIFICATION, notification: {
                        type: 'error',
                        title: error.response.data.error ? error.response.data.error : 'Some error Occured, cannot perform action!'
                    }
                });
            }
        }).finally(onDiscard);
    };
    const notList = ['Team Lead', 'Reviewer'];
    // const technologiesList = useMemo(() => {
    //     return availableType.technology.map((t) => ({ value: t.value, label: t.value }));
    // }, [availableType]);
    return (
        <Form
            {...formItemLayout}
            form={form}
            name="update"
            onFinish={onFinish}
            initialValues={{
                name: `${resource.employee.firstName} ${resource.employee.lastName}`,
                username: resource.employee.userName,
                role: resource.role
                // technologies: resource.technology.map(t => t.value)
            }}
            scrollToFirstError
        >
            <Form.Item name="name" label="Name" rules={[{ required: false }]}>
                <Input disabled />
            </Form.Item>
            <Form.Item name="username" label="User Name" rules={[{ required: false }]}>
                <Input disabled />
            </Form.Item>
            <Form.Item name="role" label="Role" rules={[{ required: true }]}>
                <Select
                    placeholder="Payment Type"
                >
                    {
                        availableType.role.filter(r => !notList.includes(r.value))
                            .map((rt, index) => rt.status ? <Option key={index} value={rt.value}>
                                <Tooltip title={rt.description}>
                                    {rt.value}
                                </Tooltip>
                            </Option> : null)
                    }
                </Select>
            </Form.Item>
            {/* <Form.Item label="Technologies">
                <Form.List name="technologies" {...formItemLayoutWithOutLabel}>
                    {(fields, { add, remove }) => (
                        <>
                            {fields.map(field => (
                                <Space key={field.key} style={{ display: 'flex', marginBottom: 8 }} align="baseline">
                                    <Form.Item
                                        {...field}
                                        rules={[{ required: true, message: 'This is required Field' }]}
                                    >
                                        <AutoComplete
                                            style={{ width: 200 }}
                                            options={technologiesList}
                                            placeholder="Select Technology"
                                            filterOption={(inputValue, option) => {
                                                return option.label.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1;
                                            }}
                                        />
                                    </Form.Item>
                                    <MinusCircleOutlined onClick={() => remove(field.name)} />
                                </Space>
                            ))}
                            <Form.Item>
                                <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />}>
                                    Add Technologies
                                </Button>
                            </Form.Item>
                        </>
                    )}
                </Form.List>
            </Form.Item> */}
            <Form.Item {...tailFormItemLayout}>
                <Button type="primary" htmlType="submit">
                    Update
                </Button>
                <Button onClick={onDiscard} style={{ marginLeft: 16 }}>
                    Discard
                </Button>
            </Form.Item>
        </Form>
    );
}

export default EditResourceInProject;
