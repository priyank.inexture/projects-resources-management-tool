import React from 'react';
import { Link } from 'react-router-dom';
import { Table, Popconfirm } from 'antd';
import { DeleteOutlined } from '@ant-design/icons';
import { currencyFormat } from '../utils/CommonUtils';
import { useGlobalStateContext, useGlobalDispatchContext } from '../context/globalContext';
import { UPDATE_NOTIFICATION } from '../constants/ActionType';
import { deleteResponsibilityData } from '../apiActions/ResponsibilityAction';
// eslint-disable-next-line react/prop-types
function ResourceProjectList({ projects, setResource }) {
    const { currency, availableType, isSuperUser } = useGlobalStateContext();
    const dispatch = useGlobalDispatchContext();
    // console.log('projects', projects);
    const deleteResource = (record) => {
        // console.log(projects);
        // console.log('delete ', record?.responsibility?._id, record._id);
        deleteResponsibilityData(record._id, record?.responsibility?._id).then(res => {
            if (res.data.responsibilities && res.data.deletedCount > 0) {

                // console.log('newprojects', projects.filter(p => p._id !== record._id));
                // eslint-disable-next-line react/prop-types,max-nested-callbacks
                setResource((prevResource) => ({ ...prevResource, projects: projects.filter(p => p._id !== record._id) }));
                dispatch({
                    type: UPDATE_NOTIFICATION, notification: {
                        type: 'success',
                        title: 'Successfully removed from project!'
                    }
                });
                // console.log(res.data);
                // setProject((prevProject) => ({ ...prevProject, responsibilities: res.data.responsibilities }));
            }
        }).catch(error => {
            if (error.response) {
                if (error.response.data.error) {
                    dispatch({
                        type: UPDATE_NOTIFICATION, notification: {
                            type: 'info',
                            title: error.response.data.error
                        }
                    });
                }
            } else {
                dispatch({
                    type: UPDATE_NOTIFICATION, notification: {
                        type: 'error',
                        title: 'Not able to delete resource from project'
                    }
                });
            }
        });
    };
    const superColumns = [
        {
            title: 'Action',
            width: 100,
            // eslint-disable-next-line react/display-name
            render: (record) => (
                <>
                    {(record.role !== 'Team Lead' && record.role !== 'Reviewer') &&
                        <>
                            <Popconfirm
                                title="Are you sure to delete this resource from project?"
                                onConfirm={() => deleteResource(record)}
                                okText="Confirm"
                                cancelText="Cancel"
                            >
                                <DeleteOutlined style={{ color: '#f81d22' }} /></Popconfirm >
                        </>
                    }
                </>
            )
        }
    ];
    const columns = [
        {
            title: 'Id',
            dataIndex: '_id',
            // eslint-disable-next-line react/display-name
            render: (text) => (<Link to={`/projects/${text}`}>{text}</Link>)
        },
        {
            title: 'Project Name',
            dataIndex: 'name',
            defaultSortOrder: 'ascend',
            sorter: (a, b) => a.name.localeCompare(b.name)
        },
        {
            title: 'Payment Type',
            dataIndex: 'paymentType',
            filters: availableType.payment.map(p => ({ text: p.value, value: p.value })),
            onFilter: (value, record) => (record.paymentType.indexOf(value) >= 0)
        },
        {
            title: 'Amount',
            dataIndex: 'amount',
            sorter: (a, b) => a.amount - b.amount,
            // eslint-disable-next-line react/display-name
            render: (text) => (<>{currencyFormat(text, currency.to, currency.toRate)}</>)
        },
        {
            title: 'Allocation',
            dataIndex: 'allocation',
            filters: availableType.allocation.map(a => ({ text: a.value, value: a.value })),
            onFilter: (value, record) => (record.allocation.indexOf(value) >= 0)
        },
        {
            title: 'Priority',
            dataIndex: 'priority',
            filters: availableType.priority.map(p => ({ text: p.value, value: p.value })),
            onFilter: (value, record) => (record.priority.indexOf(value) >= 0)
        },
        {
            title: 'Status',
            dataIndex: 'status',
            filters: availableType.status.map(s => ({ text: s.value, value: s.value })),
            onFilter: (value, record) => (record.status.indexOf(value) >= 0)
        },
        {
            title: 'Team Lead',
            dataIndex: ['teamLead'],
            // eslint-disable-next-line react/display-name
            render: (teamLead) => (<>{teamLead ? teamLead.userName : <b>Not Assigned</b>}</>)
        },
        {
            title: 'Reviewer',
            dataIndex: ['reviewer'],
            // eslint-disable-next-line react/display-name
            render: (reviewer) => (<>{reviewer ? reviewer.userName : <b>Not Assigned</b>}</>)
        },
        {
            title: 'Role',
            dataIndex: 'role',
            filters: availableType.role.map(r => ({ text: r.value, value: r.value })),
            onFilter: (value, record) => (record.role.indexOf(value) >= 0)
        },
        ...(isSuperUser ? superColumns : [])
    ];
    return (
        <Table columns={columns} dataSource={projects} rowKey="_id" />
    );
}

export default ResourceProjectList;
