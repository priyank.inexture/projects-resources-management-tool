import React, { useMemo } from 'react';
import { Form, Input, Button, Select, Tooltip, Space, AutoComplete } from 'antd';
import { PlusOutlined, MinusCircleOutlined } from '@ant-design/icons';
import { useGlobalDispatchContext, useGlobalStateContext } from '../context/globalContext';
import { postResourceData } from '../apiActions/ResourceAction';
import { UPDATE_RESOURCES, UPDATE_NOTIFICATION } from '../constants/ActionType';
const { Option } = Select;
// eslint-disable-next-line react/prop-types
function AddResource({ setAdd }) {
    const { employees, availableType } = useGlobalStateContext();
    const dispatch = useGlobalDispatchContext();
    const formItemLayout = {
        labelCol: {
            xs: {
                span: 24
            },
            sm: {
                span: 8
            }
        },
        wrapperCol: {
            xs: {
                span: 24
            },
            sm: {
                span: 8
            }
        }
    };
    const tailFormItemLayout = {
        wrapperCol: {
            xs: {
                span: 24,
                offset: 0
            },
            sm: {
                span: 16,
                offset: 8
            }
        }
    };
    const formItemLayoutWithOutLabel = {
        wrapperCol: {
            xs: { span: 24, offset: 0 },
            sm: { span: 20, offset: 4 }
        }
    };
    const technologiesList = useMemo(() => {
        return availableType.technology.map((t) => ({ value: t.value, label: t.value }));
    }, [availableType]);
    const [form] = Form.useForm();
    const onFinish = (values) => {
        console.log('Received values of form: ', values);
        postResourceData(values).then(res => {
            dispatch({ type: UPDATE_RESOURCES, employees: res.data.employees });
            dispatch({
                type: UPDATE_NOTIFICATION, notification: {
                    type: 'success',
                    title: 'Added employee successfully'
                }
            });
        }).finally(() => {
            setAdd(false);
        });
        // form.resetFields();
    };
    return (
        <Form
            {...formItemLayout}
            form={form}
            name="addResource"
            onFinish={onFinish}
            initialValues={{
                amount: 0
            }}
            scrollToFirstError
        >
            <Form.Item
                name="firstName"
                label={
                    <span>
                        First Name
                    </span>
                }
                rules={[
                    {
                        required: true,
                        whitespace: true,
                        message: 'Please input your first name!'
                    }
                ]}
            >
                <Input />
            </Form.Item>
            <Form.Item
                name="lastName"
                label={
                    <span>
                        Last Name
                    </span>
                }
                rules={[
                    {
                        required: true,
                        whitespace: true,
                        message: 'Please input your Last name!'
                    }
                ]}
            >
                <Input />
            </Form.Item>
            <Form.Item
                name="userName"
                label={
                    <span>
                        Username
                    </span>
                }
                hasFeedback
                rules={[
                    {
                        required: true,
                        whitespace: false,
                        message: 'Please input your Username!'
                    },
                    ({ getFieldValue }) => ({
                        validator(_, value) {
                            if (value.indexOf(' ') >= 0) {
                                return Promise.reject(new Error('Username must not contain white space!'));
                            } else if (employees.some(e => e.userName === value) && value !== '') {
                                return Promise.reject(new Error('Username already exists!'));
                            }
                            return Promise.resolve();
                        }
                    })
                ]}
            >
                <Input />
            </Form.Item>
            <Form.Item
                name="email"
                label={
                    <span>
                        Email
                    </span>
                }
                rules={[
                    {
                        required: true,
                        whitespace: false,
                        message: 'Please input your Email!'
                    }
                ]}
            >
                <Input />
            </Form.Item>
            <Form.Item
                name="password"
                label="Password"
                rules={[
                    {
                        required: true,
                        message: 'Please input your password!'
                    }
                ]}
                hasFeedback
            >
                <Input.Password />
            </Form.Item>

            <Form.Item
                name="confirm"
                label="Confirm Password"
                dependencies={['password']}
                hasFeedback
                rules={[
                    {
                        required: true,
                        message: 'Please confirm your password!'
                    },
                    ({ getFieldValue }) => ({
                        validator(_, value) {
                            if (!value || getFieldValue('password') === value) {
                                return Promise.resolve();
                            }
                            return Promise.reject(new Error('The two passwords that you entered do not match!'));
                        }
                    })
                ]}
            >
                <Input.Password />
            </Form.Item>
            <Form.Item
                name="contactNumber"
                label="Phone Number"
                rules={[{ required: true, message: 'Please input your contact number!' }]}
            >
                <Input />
            </Form.Item>
            <Form.Item
                name="address"
                label="Address"
                rules={[{ required: true, message: 'Please input your Address!' }]}
            >
                <Input />
            </Form.Item>
            <Form.Item name="designation" label="Designation" rules={[{ required: false }]}>
                <Select
                    placeholder="Select designation"
                >
                    {
                        availableType.designation
                            .map((dt, index) => dt.status ? <Option key={index} value={dt.value}>
                                <Tooltip title={dt.description}>
                                    {dt.value}
                                </Tooltip>
                            </Option> : null)
                    }
                </Select>
            </Form.Item>
            <Form.Item label="Technologies">
                <Form.List name="technology" {...formItemLayoutWithOutLabel}>
                    {(fields, { add, remove }) => (
                        <>
                            {fields.map(field => (
                                <Space key={field.key} style={{ display: 'flex', marginBottom: 8 }} align="baseline">
                                    <Form.Item
                                        {...field}
                                        rules={[{ required: true, message: 'This is required Field' }]}
                                    >
                                        <AutoComplete
                                            style={{ width: 200 }}
                                            options={technologiesList}
                                            placeholder="Select Technology"
                                            filterOption={(inputValue, option) => {
                                                return option.label.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1;
                                            }}
                                        />
                                    </Form.Item>
                                    <MinusCircleOutlined onClick={() => remove(field.name)} />
                                </Space>
                            ))}
                            <Form.Item>
                                <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />}>
                                    Add Technologies
                                </Button>
                            </Form.Item>
                        </>
                    )}
                </Form.List>
            </Form.Item>
            <Form.Item {...tailFormItemLayout}>
                <Button type="primary" htmlType="submit">
                    Submit
                </Button>
            </Form.Item>
        </Form>
    );
}

export default AddResource;
