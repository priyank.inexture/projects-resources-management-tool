import React from 'react';
import { Form, Input, Button } from 'antd';
import { useGlobalDispatchContext } from '../context/globalContext';
import { postClientData } from '../apiActions/ClientAction';
import { UPDATE_CLIENTS, UPDATE_NOTIFICATION } from '../constants/ActionType';
// eslint-disable-next-line react/prop-types
function AddClient({ setAdd }) {
    const dispatch = useGlobalDispatchContext();
    const formItemLayout = {
        labelCol: {
            xs: {
                span: 24
            },
            sm: {
                span: 8
            }
        },
        wrapperCol: {
            xs: {
                span: 24
            },
            sm: {
                span: 8
            }
        }
    };
    const tailFormItemLayout = {
        wrapperCol: {
            xs: {
                span: 24,
                offset: 0
            },
            sm: {
                span: 16,
                offset: 8
            }
        }
    };
    const [form] = Form.useForm();
    const onFinish = (values) => {
        // console.log('Received values of form: ', values);
        postClientData(values).then(res => {
            dispatch({ type: UPDATE_CLIENTS, clients: res.data.clients });
            dispatch({
                type: UPDATE_NOTIFICATION, notification: {
                    type: 'success',
                    title: 'Added client successfully'
                }
            });
        }).finally(() => {
            setAdd(false);
        });
        form.resetFields();
    };
    return (
        <Form
            {...formItemLayout}
            form={form}
            name="addResource"
            onFinish={onFinish}
            initialValues={{
                amount: 0
            }}
            scrollToFirstError
        >
            <Form.Item
                name="firstName"
                label={
                    <span>
                        First Name
                    </span>
                }
                rules={[
                    {
                        required: true,
                        whitespace: true,
                        message: 'Please input your first name!'
                    }
                ]}
            >
                <Input />
            </Form.Item>
            <Form.Item
                name="lastName"
                label={
                    <span>
                        Last Name
                    </span>
                }
                rules={[
                    {
                        required: true,
                        whitespace: true,
                        message: 'Please input your Last name!'
                    }
                ]}
            >
                <Input />
            </Form.Item>

            <Form.Item
                name="email"
                label={
                    <span>
                        Email
                    </span>
                }
                rules={[
                    {
                        required: true,
                        whitespace: false,
                        message: 'Please input your Email!'
                    }
                ]}
            >
                <Input />
            </Form.Item>
            <Form.Item
                name="contactNumber"
                label="Phone Number"
                rules={[{ required: true, message: 'Please input your contact number!' }]}
            >
                <Input />
            </Form.Item>
            <Form.Item
                name="address"
                label="Address"
                rules={[{ required: true, message: 'Please input your Address!' }]}
            >
                <Input />
            </Form.Item>

            <Form.Item {...tailFormItemLayout}>
                <Button type="primary" htmlType="submit">
                    Submit
                </Button>
            </Form.Item>
        </Form>
    );
}
export default AddClient;
