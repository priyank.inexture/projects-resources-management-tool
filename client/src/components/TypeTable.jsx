import React, { useState, useEffect } from 'react';
import { Switch, Button, Form, Typography } from 'antd';
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';
import { useGlobalStateContext, useGlobalDispatchContext } from '../context/globalContext';
import { deleteTypeData, putTypeData } from '../apiActions/TypeAction';
import { UPDATE_NOTIFICATION, UPDATE_AVAILABLE_TYPE, UPDATE_PROJECTS } from '../constants/ActionType';
import EditableTable from './EditableTable';
import AddType from './AddType';
const typeProp = { edit: 'edit', delete: 'delete' };
// eslint-disable-next-line react/prop-types
function TypeTable({ type, shouldDisable, setShouldDisable, notUpdateList }) {
    const { availableType } = useGlobalStateContext();
    const dispatch = useGlobalDispatchContext();
    const [form] = Form.useForm();
    const [data, setData] = useState([]);
    const [actionKey, setActionKey] = useState('');
    const [typeAction, setTypeAction] = useState('');
    const setInitialData = () => {
        // console.log(availableType);
        switch (type) {
            case 'status':
                setData(availableType.status.map(s => s.value !== 'Not Assigned' ? s : null).filter(s => s));
                break;
            case 'priority':
                setData(availableType.priority.map(s => s.value !== 'Not Assigned' ? s : null).filter(s => s));
                break;
            case 'payment':
                setData(availableType.payment.map(s => s.value !== 'Not Assigned' ? s : null).filter(s => s));
                break;
            case 'allocation':
                setData(availableType.allocation.map(s => s.value !== 'Not Assigned' ? s : null).filter(s => s));
                break;
            case 'role':
                setData(availableType.role.map(s => s.value !== 'Not Assigned' ? s : null).filter(s => s));
                break;
            case 'designation':
                setData(availableType.designation.map(s => s.value !== 'Not Assigned' ? s : null).filter(s => s));
                break;
            case 'technology':
                setData(availableType.technology.map(s => s.value !== 'Not Assigned' ? s : null).filter(s => s));
                break;
            default:
                setData([]);
                console.log('no such type exist');
                break;
        }
    };
    useEffect(() => {
        setInitialData();
    }, [availableType]);
    const isAction = (record) => (record._id === actionKey && typeAction !== typeProp.delete);
    const onEdit = (record) => {
        setTypeAction(typeProp.edit);
        form.setFieldsValue({
            value: '',
            description: '',
            status: false,
            ...record
        });
        // console.log(record);
        setActionKey(record._id);
    };
    const onDelete = (record) => {
        setTypeAction(typeProp.delete);
        // console.log('deleting', record._id);
        setActionKey(record._id);
    };
    const cancel = () => {
        setActionKey('');
        setTypeAction('');
    };

    // eslint-disable-next-line space-before-function-paren
    const saveEdit = async (key) => {
        console.log('saving edit', key);
        try {
            const row = await form.validateFields();
            putTypeData(type, row, key).then(res => {
                // const newData = [...data];
                // const index = newData.findIndex((item) => key === item._id);
                // if (index > -1) {
                //     const item = newData[index];
                //     switch (type) {
                //         case 'status':
                //             newData.splice(index, 1, { ...item, ...res.data.status });
                //             break;
                //         case 'priority':
                //             newData.splice(index, 1, { ...item, ...res.data.priority });
                //             break;
                //         case 'payment':
                //             newData.splice(index, 1, { ...item, ...res.data.payment });
                //             break;
                //         case 'allocation':
                //             newData.splice(index, 1, { ...item, ...res.data.allocation });
                //             break;
                //         default:
                //             throw new Error(`Not able to find ${type} dispatch`);
                //     }
                //     // console.log(newData);
                //     setData(newData);
                let load = [];
                switch (type) {
                    case 'status':
                        load = res.data.status;
                        break;
                    case 'priority':
                        load = res.data.priority;
                        break;
                    case 'payment':
                        load = res.data.payment;
                        break;
                    case 'allocation':
                        load = res.data.allocation;
                        break;
                    case 'role':
                        load = res.data.role;
                        break;
                    case 'designation':
                        load = res.data.designation;
                        break;
                    case 'technology':
                        load = res.data.technology;
                        break;
                    default:
                        throw new Error(`Not able to find ${type} dispatch`);
                }
                // console.log({
                //     type: UPDATE_AVAILABLE_TYPE, typeOf: type, load: load
                // });
                dispatch({
                    type: UPDATE_AVAILABLE_TYPE, typeOf: type, load: load
                });
                if (res.data.projects) {
                    dispatch({ type: UPDATE_PROJECTS, projects: res.data.projects });
                }
                dispatch({
                    type: UPDATE_NOTIFICATION, notification: {
                        type: 'success',
                        title: 'Updated Type successfully'
                    }
                });
                load = load.map(s => s.value !== 'Not Assigned' ? s : null).filter(s => s);
                setData([...load]);
                setActionKey(key);
                setTypeAction(typeProp.edit);
                setActionKey('');
                setTypeAction('');
            }).catch(error => {
                dispatch({
                    type: UPDATE_NOTIFICATION, notification: {
                        type: 'error',
                        title: error.response.data ? error.response.data.error : `Updating ${type} failed!`
                    }
                });
                console.log(error);
            });
            // console.log(row);
        } catch (error) {
            console.log(error);
        }
        setActionKey('');
        setTypeAction('');
    };

    // eslint-disable-next-line space-before-function-paren
    const saveDelete = async (key) => {
        try {
            deleteTypeData(type, key).then(res => {
                // console.log(res);
                const newData = [...data];
                const index = newData.findIndex((item) => key === item._id);
                if (index > -1 && res.data.stats.deletedCount > 0) {
                    newData.splice(index, 1);
                    setData(newData);
                    setActionKey('');
                    let load = [];
                    switch (type) {
                        case 'status':
                            load = res.data.status;
                            break;
                        case 'priority':
                            load = res.data.priority;
                            break;
                        case 'payment':
                            load = res.data.payment;
                            break;
                        case 'allocation':
                            load = res.data.allocation;
                            break;
                        case 'role':
                            load = res.data.role;
                            break;
                        case 'designation':
                            load = res.data.designation;
                            break;
                        case 'technology':
                            load = res.data.technology;
                            break;
                        default:
                            throw new Error(`Not able to find ${type} dispatch`);
                    }

                    dispatch({ type: UPDATE_AVAILABLE_TYPE, typeOf: type, load: load });
                    if (res.data.projects) {
                        dispatch({ type: UPDATE_PROJECTS, projects: res.data.projects });
                    }
                    dispatch({
                        type: UPDATE_NOTIFICATION, notification: {
                            type: 'success',
                            title: 'Deleted type successfully'
                        }
                    });
                } else {
                    setData(newData);
                    setActionKey('');
                    setTypeAction('');
                }
            }).catch(error => {
                dispatch({
                    type: UPDATE_NOTIFICATION, notification: {
                        type: 'error',
                        title: error.response.data ? error.response.data.error : `Deleting ${type} failed!`
                    }
                });
                setActionKey('');
                setTypeAction('');
                console.log(error);
            });
        } catch (errInfo) {
            console.log('Validate Failed:', errInfo);
        }
    };

    const columns = [
        // {
        //     title: 'Id',
        //     dataIndex: '_id',
        //     editable: false
        // },
        {
            title: 'Value',
            dataIndex: 'value',
            editable: true
        },
        {
            title: 'Description',
            dataIndex: 'description',
            editable: true
        },
        {
            title: 'Status',
            dataIndex: 'status',
            // eslint-disable-next-line react/display-name
            render: (statusValue) => (<Switch disabled defaultChecked={statusValue} />),
            editable: true
        },
        {
            title: 'Action',
            dataIndex: 'action',
            // eslint-disable-next-line react/display-name
            render: (_, record) => {
                const editable = isAction(record);
                return (editable || (typeAction === typeProp.delete && record._id === actionKey)) ? (
                    <span>
                        {
                            (typeAction === typeProp.edit) ? <a onClick={() => saveEdit(record._id)} style={{ marginRight: 8 }}>Save</a>
                                : <a onClick={() => saveDelete(record._id)} style={{ marginRight: 8 }}>Confirm</a>
                        }
                        <a onClick={cancel}>Cancel</a>
                    </span>
                ) : (
                    <>
                        {
                            // eslint-disable-next-line react/prop-types
                            (notUpdateList && !notUpdateList.includes(record.value)) &&
                            <>
                                <Typography.Link disabled={actionKey !== '' || shouldDisable} onClick={() => onEdit(record)}>
                                    <EditOutlined />
                                </Typography.Link>
                                <Typography.Link style={{ marginLeft: 15 }} disabled={actionKey !== '' || shouldDisable} onClick={() => onDelete(record)}>
                                    <DeleteOutlined style={{ color: (actionKey !== '' || shouldDisable) ? 'rgba(0, 0, 0, 0.25)' : '#ff4d4f' }} />
                                </Typography.Link>
                            </>
                        }
                        {
                            !notUpdateList &&
                            <>
                                <Typography.Link disabled={actionKey !== '' || shouldDisable} onClick={() => onEdit(record)}>
                                    <EditOutlined />
                                </Typography.Link>
                                <Typography.Link style={{ marginLeft: 15 }} disabled={actionKey !== '' || shouldDisable} onClick={() => onDelete(record)}>
                                    <DeleteOutlined style={{ color: (actionKey !== '' || shouldDisable) ? 'rgba(0, 0, 0, 0.25)' : '#ff4d4f' }} />
                                </Typography.Link>
                            </>
                        }
                    </>
                );
            }
        }
    ];
    const mergedColumns = columns.map((col) => {
        if (!col.editable) {
            return col;
        }
        return {
            ...col,
            onCell: (record) => ({
                record,
                inputType: col.dataIndex !== 'status' ? 'text' : 'boolean',
                dataIndex: col.dataIndex,
                title: col.title,
                editing: isAction(record)
            })
        };
    });
    const [add, setAdd] = useState(false);
    return (
        <>
            {!add &&
                <div>
                    <Button
                        style={{ marginBottom: 5 }}
                        onClick={() => {
                            setAdd(true);
                            setShouldDisable(true);
                        }} type="primary" disabled={actionKey !== '' || shouldDisable}>Add</Button>
                    <EditableTable
                        columns={columns}
                        mergedColumns={mergedColumns}
                        data={data}
                        form={form}
                        cancel={cancel}
                    // PositionNone
                    />
                    {/* <Table columns={columns} dataSource={employees} rowKey="_id" /> */}
                </div>
            }
            {
                add &&
                <div>
                    <Button onClick={() => { setAdd(false); setShouldDisable(false); }} >Back</Button>
                    <AddType setAdd={setAdd} type={type} setShouldDisable={setShouldDisable} />
                </div>
            }
        </>
    );
}
export default TypeTable;
