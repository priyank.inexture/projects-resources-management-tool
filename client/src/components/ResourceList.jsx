/* eslint-disable react/prop-types */
import React, { useState } from 'react';
import { Button, Table, Typography, Popconfirm } from 'antd';
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import { useParams, Link } from 'react-router-dom';
import { deleteResponsibilityData } from '../apiActions/ResponsibilityAction';
import { useGlobalDispatchContext, useGlobalStateContext } from '../context/globalContext';
import { UPDATE_NOTIFICATION } from '../constants/ActionType';
import EditResourceInProject from './EditResourceInProject';
import AddResourceToProject from './AddResourceToProject';
function ResourceList({ list, setProject }) {
    let { id } = useParams();
    const dispatch = useGlobalDispatchContext();
    const { availableType, isSuperUser } = useGlobalStateContext();
    const [add, setAdd] = useState(false);
    const [edit, setEdit] = useState(false);
    const [selectedRecord, setSelectedRecord] = useState(null);
    const superColumns = [
        {
            title: 'Action',
            width: 100,
            // eslint-disable-next-line react/display-name
            render: (record) => (
                <>
                    {(record.role !== 'Team Lead' && record.role !== 'Reviewer') &&
                        <>
                            <Typography.Link disabled={selectedRecord !== null}>
                                <EditOutlined onClick={() => onEdit(record)} />
                            </Typography.Link>
                            <Typography.Link style={{ marginLeft: 15 }} disabled={selectedRecord !== null}>
                                <Popconfirm
                                    onConfirm={() => deleteResourceFromList(record)}
                                    title="Are you sure to remove this resource from project?"
                                >
                                    <DeleteOutlined style={{ color: selectedRecord ? 'rgba(0, 0, 0, 0.25)' : '#f81d22' }} />
                                </Popconfirm>

                            </Typography.Link>
                        </>
                    }
                </>
            )
        }
    ];
    const columns = [
        {
            title: 'Id',
            dataIndex: 'employee',
            // eslint-disable-next-line react/display-name
            render: (employee) => (<Link to={`/resources/${employee._id}`}>{employee._id}</Link>)
        },
        {
            title: 'Name',
            dataIndex: 'employee',
            defaultSortOrder: 'ascend',
            sorter: (a, b) => {
                let aName = `${a.employee.firstName} ${a.employee.lastName}`;
                let bName = `${b.employee.firstName} ${b.employee.lastName}`;
                return aName.localeCompare(bName);
            },
            // eslint-disable-next-line react/display-name
            render: (employee) => (<>{employee.firstName} {employee.lastName}</>)
        },
        {
            title: 'Username',
            dataIndex: 'employee',
            sorter: (a, b) => a.employee.userName.localeCompare(b.employee.userName),
            // eslint-disable-next-line react/display-name
            render: (employee) => (<>{employee.userName}</>)
        },
        {
            title: 'Email',
            dataIndex: 'employee',
            // eslint-disable-next-line react/display-name
            render: (employee) => (<a href={`mailto:${employee.email}`}>{employee.email}</a>)
        },
        {
            title: 'Designation',
            dataIndex: 'employee',
            filters: availableType.designation.map(d => ({ text: d.value, value: d.value })),
            onFilter: (value, record) => (record.employee.designation.indexOf(value) >= 0),
            // eslint-disable-next-line react/display-name
            render: (employee) => (<>{employee.designation}</>)
        },
        {
            title: 'Technology',
            dataIndex: 'employee',
            filters: availableType.technology.map(t => ({ text: t.value, value: t.value })),
            onFilter: (value, record) => {
                let s = record.employee.technology.map(t => availableType.technology.find(tech => tech._id === t) ? availableType.technology.find(tech => tech._id === t).value : '').join(', ');
                return s.indexOf(value) >= 0;
            },
            // eslint-disable-next-line react/display-name
            render: (employee) => (<>{employee.technology.map(t => availableType.technology.find(tech => tech._id === t) ? availableType.technology.find(tech => tech._id === t).value : '').join(', ')}</>)
        },
        {
            title: 'Role',
            dataIndex: 'role',
            filters: availableType.role.map(r => ({ text: r.value, value: r.value })),
            onFilter: (value, record) => (record.role.indexOf(value) >= 0)
        },
        ...(isSuperUser ? superColumns : [])
    ];
    const onEdit = (record) => {
        setEdit(true);
        setSelectedRecord(record);
        // console.log('editing ', record);
    };
    const deleteResourceFromList = (record) => {
        deleteResponsibilityData(id, record._id).then(res => {
            if (res.data.responsibilities && res.data.deletedCount > 0) {
                // console.log(res.data);
                setProject((prevProject) => ({ ...prevProject, responsibilities: res.data.responsibilities }));
            }
            dispatch({
                type: UPDATE_NOTIFICATION, notification: {
                    type: 'success',
                    title: `Successfully removed ${record.employee.firstName} ${record.employee.lastName}`
                }
            });
        }).catch(error => {
            if (error.response) {
                if (error.response.data.error) {
                    dispatch({
                        type: UPDATE_NOTIFICATION, notification: {
                            type: 'info',
                            title: error.response.data.error
                        }
                    });
                }
            } else {
                dispatch({
                    type: UPDATE_NOTIFICATION, notification: {
                        type: 'error',
                        title: 'Not able to delete resource from project'
                    }
                });
            }
        });
    };
    const onDiscard = () => {
        setEdit(false);
        setSelectedRecord(null);
    };
    return (
        <>
            {
                (edit && isSuperUser) && <>
                    {/* <Button type="default" onClick={() => { setEdit(false); setSelectedRecord(null); }}>
                        back
                    </Button> */}
                    <EditResourceInProject resource={selectedRecord} onDiscard={onDiscard} setProject={setProject} responsibilities={list} />
                </>
            }
            {(!edit && add && isSuperUser) &&
                <>
                    <Button type="default" onClick={() => setAdd(false)}>
                        back
                    </Button>
                    <AddResourceToProject setAdd={setAdd} project={id} setProject={setProject} />
                </>
            }
            {(!edit && !add) &&
                <>

                    { isSuperUser && <Button type="primary" onClick={() => setAdd(true)}>Add</Button>}
                    <Table columns={columns} dataSource={list} rowKey="_id" />
                </>
            }
        </>
    );
}

export default ResourceList;
