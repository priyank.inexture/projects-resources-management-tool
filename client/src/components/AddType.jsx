import React, { useEffect } from 'react';
import { Form, Input, Button, Switch } from 'antd';
import { useGlobalDispatchContext } from '../context/globalContext';
import { postTypeData } from '../apiActions/TypeAction';
import { UPDATE_NOTIFICATION, UPDATE_AVAILABLE_TYPE } from '../constants/ActionType';
// eslint-disable-next-line react/prop-types
function AddType({ setAdd, type, setShouldDisable }) {
    const dispatch = useGlobalDispatchContext();
    const formItemLayout = {
        labelCol: {
            xs: {
                span: 24
            },
            sm: {
                span: 8
            }
        },
        wrapperCol: {
            xs: {
                span: 24
            },
            sm: {
                span: 8
            }
        }
    };
    const tailFormItemLayout = {
        wrapperCol: {
            xs: {
                span: 24,
                offset: 0
            },
            sm: {
                span: 16,
                offset: 8
            }
        }
    };
    const [form] = Form.useForm();

    const onFinish = (values) => {
        // console.log('Received values of form: ', values);
        postTypeData(type, values).then(res => {
            let load = [];
            switch (type) {
                case 'status':
                    load = res.data.status;
                    break;
                case 'priority':
                    load = res.data.priority;
                    break;
                case 'payment':
                    load = res.data.payment;
                    break;
                case 'allocation':
                    load = res.data.allocation;
                    break;
                case 'role':
                    load = res.data.role;
                    break;
                case 'designation':
                    load = res.data.designation;
                    break;
                case 'technology':
                    load = res.data.technology;
                    break;
                default:
                    throw new Error(`Not able to find ${type} dispatch`);
            }
            dispatch({ type: UPDATE_AVAILABLE_TYPE, typeOf: type, load: load });
            dispatch({
                type: UPDATE_NOTIFICATION, notification: {
                    type: 'success',
                    title: 'Added type successfully'
                }
            });
        }).finally(() => {
            setShouldDisable(false);
            setAdd(false);
        });
        form.resetFields();
    };
    useEffect(() => {
        form.setFieldsValue({
            value: '',
            description: '',
            status: false
        });
    }, [form]);
    return (
        <Form
            {...formItemLayout}
            form={form}
            name="addType"
            onFinish={onFinish}
            initialValues={{
                amount: 0
            }}
            scrollToFirstError
        >
            <Form.Item
                name="value"
                label={
                    <span>
                        Value
                    </span>
                }
                rules={[
                    {
                        required: true,
                        whitespace: true,
                        message: 'Please input your new type!'
                    }
                ]}
            >
                <Input />
            </Form.Item>
            <Form.Item
                name="description"
                label={
                    <span>
                        Description
                    </span>
                }
                rules={[
                    {
                        required: true,
                        whitespace: true,
                        message: 'Please input your type description!'
                    }
                ]}
            >
                <Input />
            </Form.Item>
            <Form.Item
                name="status"
                valuePropName='checked'
                label={
                    <span>
                        Status
                    </span>
                }
            >
                <Switch />
            </Form.Item>
            <Form.Item {...tailFormItemLayout}>
                <Button type="primary" htmlType="submit">
                    Submit
                </Button>
            </Form.Item>
        </Form>
    );
}
export default AddType;
