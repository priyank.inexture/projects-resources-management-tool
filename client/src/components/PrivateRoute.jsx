/* eslint-disable react/prop-types */
import React from 'react';
import {
    Route,
    Redirect
} from 'react-router-dom';
import { useGlobalStateContext } from '../context/globalContext';
export default function PrivateRoute({ Component, ...rest }) {
    const { isAuthenticated } = useGlobalStateContext();
    return (
        <Route
            {...rest}
            render={props =>
                isAuthenticated ? (
                    <Component {...props} />
                ) : (
                    <Redirect
                        to={{
                            pathname: '/login',
                            state: { from: props.location }
                        }}
                    />
                )
            }
        />
    );
}
