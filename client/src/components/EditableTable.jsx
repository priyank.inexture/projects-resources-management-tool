import React from 'react';
import { Table, Input, Form, Switch, Select } from 'antd';
const EditableCell = ({
    // eslint-disable-next-line react/prop-types
    editing, dataIndex, title, inputType, options, record, index, children,
    ...restProps
}) => {
    // eslint-disable-next-line no-nested-ternary,react/prop-types
    let inputNode = null;
    if (editing) {
        console.log(dataIndex);
    }
    switch (inputType) {
        case 'email':
            inputNode = < Input type="email" />;
            break;
        case 'boolean':
            inputNode = <Switch defaultChecked={record[dataIndex]} />;
            break;
        case 'select':
            inputNode = <Select options={options ? options : []} />;
            break;
        default:
            inputNode = <Input />;
            break;
    }
    const valuePropName = inputType === 'boolean' ? { valuePropName: 'checked' } : null;
    return (
        <td {...restProps}>
            {editing ? (
                <Form.Item
                    name={dataIndex}
                    style={{
                        margin: 0
                    }}
                    rules={[
                        {
                            required: true,
                            message: `Please Input ${title}!`
                        }
                    ]}
                    {...valuePropName}
                >
                    {inputNode}
                </Form.Item>
            ) : (
                children
            )
            }
        </td >
    );
};

// eslint-disable-next-line react/prop-types
function EditableTable({ form, data, mergedColumns, cancel, PositionNone, onChange }) {
    return (
        <Form form={form} component={false}>
            <Table
                components={{
                    body: {
                        cell: EditableCell
                    }
                }}
                bordered
                dataSource={data}
                columns={mergedColumns}
                onChange={onChange}
                rowClassName="editable-row"
                pagination={{
                    onChange: cancel,
                    position: PositionNone ? ['none'] : ['bottomRight']
                }}
                rowKey="_id"
            />
        </Form>
    );
}
export default EditableTable;
