import axios from 'axios';
const onCatchUnathorized = () => {
    if (window.location.href.includes('github')) {window.location = 'https://pcinx.github.io/resource-managment-app';}
    else {window.location = '/';}
    localStorage.removeItem('token');
};
export const apiPost = (url, data,options) => {
    return new Promise((resolve,reject) => {
        axios.post(url, data, {
            ...options,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('token')
            }
        })
            .then(response => resolve(response))
            .catch(error => {
                if (error.response.status === 403) {onCatchUnathorized();}
                reject(error);
            });
    });
};
export const apiGet = (url) => {
    return new Promise((resolve,reject) => {
        axios.get(url,{ headers: {
            Authorization: 'Bearer ' + localStorage.getItem('token')
        } })
            .then(response => resolve(response))
            .catch(error => {
                if (error.response.status === 403) {onCatchUnathorized();}
                reject(error);
            });
    });
};
export const apiGetNoAuthorization = (url) => {
    return new Promise((resolve,reject) => {
        axios.get(url)
            .then(response => resolve(response))
            .catch(error => {
                if (error.response.status === 403) {onCatchUnathorized();}
                reject(error);
            });
    });
};
export const apiPut = (url, data,options) => {
    return new Promise((resolve,reject) => {
        axios.put(url, data,{
            ...options,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('token')
            }
        })
            .then(response => resolve(response))
            .catch(error => {
                if (error.response.status === 403) {onCatchUnathorized();}
                reject(error);
            });
    });
};
export const apiDelete = (url, data,options) => {
    return new Promise((resolve,reject) => {
        axios.delete(url,{
            ...options,
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('token')
            }
        },{ data: data })
            .then(response => resolve(response))
            .catch(error => {
                if (error.response.status === 403) {onCatchUnathorized();}
                reject(error);
            });
    });
};
