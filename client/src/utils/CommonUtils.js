import CurrencySymbol from './CurrencySymbol';
export const currencyFormat = (value,to,toRate) => {

    let cvalue = Math.ceil(value * toRate);
    let x = cvalue.toString();
    let lastThree = x.substring(x.length - 3);
    let otherNumbers = x.substring(0, x.length - 3);
    if (otherNumbers !== '') {
        lastThree = ',' + lastThree;
    }
    if (to === 'INR') {return `${CurrencySymbol[to].symbol} ${otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ',') + lastThree}`;}
    else {return `${CurrencySymbol[to].symbol} ${otherNumbers.replace(/\B(?=(\d{3})+(?!\d))/g, ',') + lastThree}`;}
    // return `$ ${otherNumbers.replace(/\B(?=(\d{3})+(?!\d))/g, ',') + lastThree}`;

};
