import { apiPost,apiPut,apiDelete,apiGet } from '../utils/ApiUtils';
import { RESOURCE_URL } from '../constants/ApiConstants';
export const postResourceData = (data) => apiPost(RESOURCE_URL,data);
export const putResourceData = (data,id) => apiPut(`${RESOURCE_URL}/${id}`,data);
export const deleteResourceData = (id) => apiDelete(`${RESOURCE_URL}/${id}`);
export const getResourceData = (id) => apiGet(`${RESOURCE_URL}/${id}`);
