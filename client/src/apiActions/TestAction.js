import { apiGet } from '../utils/ApiUtils';
import { TEST_URL } from '../constants/ApiConstants';
export const fetchTestData = () => apiGet(TEST_URL);
