import { apiGet,apiPost,apiPut,apiDelete } from '../utils/ApiUtils';
import { CLIENT_URL } from '../constants/ApiConstants';
export const getClientData = (id) => apiGet(`${CLIENT_URL}/${id}`);
export const postClientData = (data) => apiPost(CLIENT_URL,data);
export const putClientData = (data,id) => apiPut(`${CLIENT_URL}/${id}`,data);
export const deleteClientData = (id) => apiDelete(`${CLIENT_URL}/${id}`);
