import { apiGetNoAuthorization, apiPost } from '../utils/ApiUtils';
import { CURRENCY_URL,LOGIN_URL } from '../constants/ApiConstants';
export const fetchCurrencyRates = () => apiGetNoAuthorization(CURRENCY_URL);
export const postLoginCredentials = (data) => apiPost(LOGIN_URL,data);
