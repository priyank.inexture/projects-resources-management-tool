import { apiPost,apiPut,apiDelete } from '../utils/ApiUtils';
import { RESPONSIBILITY_URL } from '../constants/ApiConstants';
export const postResponsibilityData = (data) => apiPost(RESPONSIBILITY_URL,data);
export const putResponsibilityData = (data,id) => apiPut(`${RESPONSIBILITY_URL}/${id}`,data);
export const deleteResponsibilityData = (projectId,id) => apiDelete(`${RESPONSIBILITY_URL}/${id}`,{ projectId: projectId });
