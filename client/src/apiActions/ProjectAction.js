import { apiPost,apiGet, apiPut,apiDelete } from '../utils/ApiUtils';
import { PROJECT_URL } from '../constants/ApiConstants';
export const postProjectData = (data) => apiPost(PROJECT_URL,data);
export const putProjectData = (data,id) => apiPut(`${PROJECT_URL}/${id}`,data);
export const deleteProject = (id) => apiDelete(`${PROJECT_URL}/${id}`);
export const getProjectInfo = (id) => apiGet(`${PROJECT_URL}/${id}`);
export const addClientToProject = (id,clientId) => apiPut(`${PROJECT_URL}/clients/${id}`, { clientId: clientId });
export const removeClientFromProject = (projectId,clientId) => apiDelete(`${PROJECT_URL}/${projectId}/clients/${clientId}`);
