import { apiPost,apiPut,apiDelete } from '../utils/ApiUtils';
import { TYPE_URL } from '../constants/ApiConstants';
export const postTypeData = (TYPE,data) => apiPost(`${TYPE_URL}/${TYPE}`,data);
export const putTypeData = (TYPE,data,id) => apiPut(`${TYPE_URL}/${TYPE}/${id}`,data);
export const deleteTypeData = (TYPE,id) => apiDelete(`${TYPE_URL}/${TYPE}/${id}`);
