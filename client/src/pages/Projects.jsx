import React, { useEffect, useState } from 'react';
import { Button, Table, Popconfirm } from 'antd';
import { useGlobalStateContext, useGlobalDispatchContext } from '../context/globalContext';
import { currencyFormat } from '../utils/CommonUtils';
import { Link } from 'react-router-dom';
import { deleteProject } from '../apiActions/ProjectAction';
import { DeleteOutlined } from '@ant-design/icons';
import { UPDATE_PROJECTS, UPDATE_NOTIFICATION, UPDATE_HEADER_TITLE, UPDATE_MENU_SELECTED_KEY } from '../constants/ActionType';
import AddProject from '../components/AddProject';
function Projects() {
    const { projects, currency, availableType, isSuperUser } = useGlobalStateContext();
    const dispatch = useGlobalDispatchContext();
    const handleDeleteProject = (record) => {
        deleteProject(record._id).then((res) => {
            if (res.data.deletedCount > 0) {
                dispatch({ type: UPDATE_PROJECTS, projects: res.data.projects });
                dispatch({
                    type: UPDATE_NOTIFICATION, notification: {
                        type: 'success',
                        title: 'Deleted project successfully'
                    }
                });
            }
        }).catch(error => {
            console.log(error);
            dispatch({
                type: UPDATE_NOTIFICATION, notification: {
                    type: 'error',
                    title: 'Not able to deleted project'
                }
            });
        });
    };
    const superColumns = [{
        title: 'Action',
        width: 100,
        // eslint-disable-next-line react/display-name
        render: (record) => (<Popconfirm
            title="Are you sure to delete this project?"
            onConfirm={() => handleDeleteProject(record)}
            okText="Confirm"
            cancelText="Cancel"
        >
            <DeleteOutlined style={{ color: '#f81d22' }} /></Popconfirm >)
    }];
    const columns = [
        {
            title: 'Id',
            dataIndex: '_id',
            // eslint-disable-next-line react/display-name
            render: (text) => (<Link to={`projects/${text}`}>{text}</Link>)
        },
        {
            title: 'Project Name',
            dataIndex: 'name',
            defaultSortOrder: 'ascend',
            sorter: (a, b) => a.name.localeCompare(b.name)
        },
        {
            title: 'Payment Type',
            dataIndex: 'paymentType',
            filters: availableType.payment.map(p => ({ text: p.value, value: p.value })),
            onFilter: (value, record) => (record.paymentType.indexOf(value) >= 0)
        },
        {
            title: 'Amount',
            dataIndex: 'amount',
            sorter: (a, b) => a.amount - b.amount,
            // eslint-disable-next-line react/display-name
            render: (text) => (<>{currencyFormat(text, currency.to, currency.toRate)}</>)
        },
        {
            title: 'Allocation',
            dataIndex: 'allocation',
            filters: availableType.allocation.map(a => ({ text: a.value, value: a.value })),
            onFilter: (value, record) => (record.allocation.indexOf(value) >= 0)
        },
        {
            title: 'Priority',
            dataIndex: 'priority',
            filters: availableType.priority.map(p => ({ text: p.value, value: p.value })),
            onFilter: (value, record) => (record.priority.indexOf(value) >= 0)
        },
        {
            title: 'Status',
            dataIndex: 'status',
            filters: availableType.status.map(s => ({ text: s.value, value: s.value })),
            onFilter: (value, record) => (record.status.indexOf(value) >= 0)
        },
        {
            title: 'Team Lead',
            dataIndex: ['teamLead'],
            // eslint-disable-next-line react/display-name
            render: (teamLead) => (<>{teamLead ? teamLead.userName : <b>Not Assigned</b>}</>)
        },
        {
            title: 'Reviewer',
            dataIndex: ['reviewer'],
            // eslint-disable-next-line react/display-name
            render: (reviewer) => (<>{reviewer ? reviewer.userName : <b>Not Assigned</b>}</>)
        },
        ...(isSuperUser ? superColumns : [])
    ];
    const [add, setAdd] = useState(false);
    useEffect(() => {
        // eslint-disable-next-line no-unused-expressions
        add ? dispatch({ type: UPDATE_HEADER_TITLE, headerTitle: 'Add Project' }) : dispatch({ type: UPDATE_HEADER_TITLE, headerTitle: 'List of Projects' });
    }, [add]);
    useEffect(() => {
        dispatch({ type: UPDATE_MENU_SELECTED_KEY, menuSelectedKey: '2' });
    }, []);
    return (
        <>
            {!add &&
                <div>
                    {isSuperUser && <Button style={{ marginBottom: 5 }} onClick={() => setAdd(true)} type="primary" >Add</Button>}
                    <Table columns={columns} dataSource={projects} rowKey="_id" />
                </div>
            }
            {
                add &&
                <div>
                    {isSuperUser && <Button style={{ marginBottom: 5 }} onClick={() => setAdd(false)} >Back</Button>}
                    <AddProject setAdd={setAdd} />
                </div>
            }
        </>
    );
}

export default Projects;
