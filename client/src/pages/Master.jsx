import React, { useState, useEffect } from 'react';
import { Tabs } from 'antd';
import TypeTable from '../components/TypeTable';
import { UPDATE_HEADER_TITLE } from '../constants/ActionType';
import { useGlobalDispatchContext } from '../context/globalContext';
const { TabPane } = Tabs;
const typeTableList = [
    { tab: 'Status', type: 'status', notUpdateList: [] },
    { tab: 'Priority', type: 'priority', notUpdateList: [] },
    { tab: 'Payment', type: 'payment', notUpdateList: [] },
    { tab: 'Allocation', type: 'allocation', notUpdateList: [] },
    { tab: 'Role', type: 'role', notUpdateList: ['Team Lead', 'Reviewer'] },
    { tab: 'Designation', type: 'designation', notUpdateList: [] },
    { tab: 'Technology', type: 'technology', notUpdateList: [] }
];
function Master() {
    const [shouldDisable, setShouldDisable] = useState(false);
    const [activeKey, setactiveKey] = useState(1);
    const dispatch = useGlobalDispatchContext();
    const changeTab = (activekey) => {
        setactiveKey(activekey);
    };
    useEffect(() => {
        dispatch({ type: UPDATE_HEADER_TITLE, headerTitle: `Master Tables | ${typeTableList[activeKey - 1].tab}` });
    }, [activeKey]);
    return (
        <>
            <Tabs defaultActiveKey="1" onChange={changeTab}>
                {
                    typeTableList.map((t, index) => <TabPane tab={t.tab} key={index + 1}><TypeTable type={t.type} shouldDisable={shouldDisable} setShouldDisable={setShouldDisable} notUpdateList={t.notUpdateList} /></TabPane>)
                }
            </Tabs>
        </>
    );
}

export default Master;
