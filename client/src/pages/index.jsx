import React from 'react';
import ReactDOM from 'react-dom';
import App from '../components/App';
// import 'antd/dist/antd.less';
import '../styles/index.scss';
import { GlobalProvider } from '../context/globalContext';
ReactDOM.render(<GlobalProvider><App /></GlobalProvider>, document.getElementById('root'));
