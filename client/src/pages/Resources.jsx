import React, { useState, useEffect } from 'react';
import { Button, Form, Typography } from 'antd';
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';
import { Link } from 'react-router-dom';
import { useGlobalDispatchContext, useGlobalStateContext } from '../context/globalContext';
import { UPDATE_PROJECTS, UPDATE_RESOURCES, UPDATE_NOTIFICATION, UPDATE_HEADER_TITLE } from '../constants/ActionType';
import { putResourceData, deleteResourceData } from '../apiActions/ResourceAction';
import EditableTable from '../components/EditableTable';
import AddResource from '../components/AddResource';
const typeProp = { edit: 'edit', delete: 'delete' };
function Resources() {
    const { employees, availableType } = useGlobalStateContext();
    const dispatch = useGlobalDispatchContext();
    const [form] = Form.useForm();
    const [data, setData] = useState(employees);
    const [actionKey, setActionKey] = useState('');
    const [typeAction, setTypeAction] = useState('');
    useEffect(() => {
        setData(employees);
    }, [employees]);
    const isAction = (record) => (record._id === actionKey && typeAction !== typeProp.delete);
    const onEdit = (record) => {
        setTypeAction(typeProp.edit);
        form.setFieldsValue({
            firstName: '',
            lastName: '',
            email: '',
            contactNumber: '',
            address: '',
            ...record
        });
        console.log(record);
        setActionKey(record._id);
    };
    const onDelete = (record) => {
        setTypeAction(typeProp.delete);
        console.log('deleting', record);
        setActionKey(record._id);
    };
    const cancel = () => {
        setActionKey('');
        setTypeAction('');
    };

    // eslint-disable-next-line space-before-function-paren
    const saveEdit = async (key) => {
        try {
            const row = await form.validateFields();
            // console.log(key);
            putResourceData(row, key).then(res => {
                const newData = [...data];
                const index = newData.findIndex((item) => key === item._id);
                if (index > -1) {
                    const item = newData[index];
                    // newData.splice(index, 1, { ...item, ...row });
                    // console.log("row", row);
                    // console.log("res.client", res.data.client);
                    newData.splice(index, 1, { ...item, ...res.data.employee });
                    setActionKey('');
                    dispatch({ type: UPDATE_RESOURCES, employees: newData });
                    dispatch({
                        type: UPDATE_NOTIFICATION, notification: {
                            type: 'success',
                            title: 'Updated resource successfully'
                        }
                    });
                } else {
                    newData.push(row);
                    setData(newData);
                    setActionKey('');
                }
            }).catch(error => {
                dispatch({
                    type: UPDATE_NOTIFICATION, notification: {
                        type: 'error',
                        title: 'Updating resource failed!'
                    }
                });
                setActionKey('');
                console.log(error);
            });
        } catch (errInfo) {
            console.log('Validate Failed:', errInfo);
        }
    };
    // eslint-disable-next-line space-before-function-paren
    const saveDelete = async (key) => {
        try {
            deleteResourceData(key).then(res => {
                console.log(res);
                const newData = [...data];
                const index = newData.findIndex((item) => key === item._id);
                if (index > -1 && res.data.stats.deletedCount > 0) {
                    newData.splice(index, 1);
                    setData(newData);
                    setActionKey('');
                    dispatch({ type: UPDATE_RESOURCES, employees: newData });
                    dispatch({ type: UPDATE_PROJECTS, projects: res.data.projects });
                    dispatch({
                        type: UPDATE_NOTIFICATION, notification: {
                            type: 'success',
                            title: 'Deleted resource successfully'
                        }
                    });
                } else {
                    setData(newData);
                    setActionKey('');
                    setTypeAction('');
                }
            }).catch(error => {
                dispatch({
                    type: UPDATE_NOTIFICATION, notification: {
                        type: 'error',
                        title: error.response.data ? error.response.data.error : 'Deleting resource failed!'
                    }
                });
                setActionKey('');
                setTypeAction('');
                console.log(error);
            });
        } catch (errInfo) {
            console.log('Validate Failed:', errInfo);
        }
    };

    const columns = [
        {
            title: 'Id',
            dataIndex: '_id',
            editable: false,
            // eslint-disable-next-line react/display-name
            render: (text) => (<Link to={`/resources/${text}`}>{text}</Link>)
        },
        {
            title: 'Username',
            dataIndex: 'userName',
            defaultSortOrder: 'ascend',
            sorter: (a, b) => a.userName.localeCompare(b.userName),
            editable: false
        },
        {
            title: 'First Name',
            dataIndex: 'firstName',
            sorter: (a, b) => a.firstName.localeCompare(b.firstName),
            editable: true
        },
        {
            title: 'Last  Name',
            dataIndex: 'lastName',
            sorter: (a, b) => a.lastName.localeCompare(b.lastName),
            editable: true
        },
        {
            title: 'Email',
            dataIndex: 'email',
            editable: true,
            // eslint-disable-next-line react/display-name
            render: (text) => (<a href={`mailto:${text}`}>{text}</a>)

        },
        {
            title: 'Contact Number',
            dataIndex: 'contactNumber',
            editable: true,
            // eslint-disable-next-line react/display-name
            render: (text) => (<a href={`tel:${text}`}>{text}</a>)
        },
        {
            title: 'Address',
            dataIndex: 'address',
            editable: true
        },
        {
            title: 'Technologies',
            dataIndex: 'technology',
            editable: false,
            filters: availableType.technology.map(t => ({ text: t.value, value: t.value })),
            onFilter: (value, record) => {
                let s = record.technology.map(t => availableType.technology.find(tech => tech._id === t) ? availableType.technology.find(tech => tech._id === t).value : '').join(', ');
                return s.indexOf(value) >= 0;
            },
            // eslint-disable-next-line react/display-name
            render: (technology) => (<>{technology.map(t => availableType.technology.find(tech => tech._id === t) ? availableType.technology.find(tech => tech._id === t).value : '').join(', ')}</>)
        },
        {
            title: 'Designation',
            dataIndex: 'designation',
            filters: availableType.designation.map(d => ({ text: d.value, value: d.value })),
            onFilter: (value, record) => (record.designation.indexOf(value) >= 0),
            editable: true
        },
        {
            title: 'Action',
            dataIndex: 'action',
            // eslint-disable-next-line react/display-name
            render: (_, record) => {
                const editable = isAction(record);
                return (editable || (typeAction === typeProp.delete && record._id === actionKey)) ? (
                    <span>
                        {
                            (typeAction === typeProp.edit) ? <a onClick={() => saveEdit(record._id)} style={{ marginRight: 8 }}>Save</a>
                                : <a onClick={() => saveDelete(record._id)} style={{ marginRight: 8 }}>Confirm</a>
                        }
                        <a onClick={cancel}>Cancel</a>
                    </span>
                ) : (
                    <>
                        <Typography.Link disabled={actionKey !== ''} onClick={() => onEdit(record)}>
                            <EditOutlined />
                        </Typography.Link>
                        <Typography.Link style={{ marginLeft: 15 }} disabled={actionKey !== ''} onClick={() => onDelete(record)}>
                            <DeleteOutlined style={{ color: actionKey === '' ? '#ff4d4f' : 'rgba(0, 0, 0, 0.25)' }} />
                        </Typography.Link>
                    </>
                );
            }
        }
    ];
    const mergedColumns = columns.map((col) => {
        if (!col.editable) {
            return col;
        }
        return {
            ...col,
            onCell: (record) => ({
                record,
                // eslint-disable-next-line no-nested-ternary
                inputType: col.dataIndex === 'email' ? 'email' : (col.dataIndex === 'designation' ? 'select' : 'text'),
                dataIndex: col.dataIndex,
                ...(col.dataIndex === 'designation' && { options: availableType.designation }),
                title: col.title,
                editing: isAction(record)
            })
        };
    });
    const [add, setAdd] = useState(false);
    useEffect(() => {
        // eslint-disable-next-line no-unused-expressions
        add ? dispatch({ type: UPDATE_HEADER_TITLE, headerTitle: 'Add Resource' }) : dispatch({ type: UPDATE_HEADER_TITLE, headerTitle: 'List of Resources' });
    }, [add]);
    function onChange(pagination, filters, sorter, extra) {
        console.log('params', pagination, filters, sorter, extra);
    }
    return (
        <>
            {!add &&
                <div>
                    <Button onClick={() => setAdd(true)} type="primary" disabled={actionKey !== ''} style={{ marginBottom: 5 }}>Add</Button>
                    <EditableTable
                        columns={columns}
                        mergedColumns={mergedColumns}
                        data={data}
                        form={form}
                        cancel={cancel}
                        onChange={onChange}

                    />
                    {/* <Table columns={columns} dataSource={employees} rowKey="_id" /> */}
                </div>
            }
            {
                add &&
                <div>
                    <Button onClick={() => setAdd(false)} style={{ marginBottom: 5 }}>Back</Button>
                    <AddResource setAdd={setAdd} />
                </div>
            }
        </>
    );
}

export default Resources;
