/* eslint-disable max-nested-callbacks */
import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { Form, Input, Button, Row, Col, Tabs } from 'antd';
import { EditOutlined } from '@ant-design/icons';
import { useGlobalStateContext, useGlobalDispatchContext } from '../context/globalContext';
import { getClientData, putClientData } from '../apiActions/ClientAction';
import { UPDATE_HEADER_TITLE, UPDATE_NOTIFICATION, UPDATE_LOADING, UPDATE_CLIENTS, UPDATE_MENU_SELECTED_KEY } from '../constants/ActionType';
import ClientProjectList from '../components/ClientProjectList';
const { TabPane } = Tabs;
// eslint-disable-next-line react/prop-types
function ClientDetail() {
    let { id } = useParams();
    const dispatch = useGlobalDispatchContext();
    const { clients, projects, isSuperUser } = useGlobalStateContext();
    const [isEditable, setIsEditable] = useState(false);
    const [client, setClient] = useState({
        firstName: '',
        lastName: '',
        email: '',
        contactNumber: '',
        address: '',
        projects: []
    });
    const formItemLayout = {
        labelCol: {
            xs: {
                span: 24
            },
            sm: {
                span: 8
            }
        },
        wrapperCol: {
            xs: {
                span: 24
            },
            sm: {
                span: 8
            }
        }
    };
    const tailFormItemLayout = {
        wrapperCol: {
            xs: {
                span: 24,
                offset: 0
            },
            sm: {
                span: 16,
                offset: 8
            }
        }
    };
    useEffect(() => {
        dispatch({ type: UPDATE_LOADING, loading: true });
        getClientData(id)
            .then(({ data }) => {
                console.log(data);
                setClient({
                    ...data.client,
                    projects: data.projects.map(r => projects.find(p => p._id === r._id)).filter(p => p)
                });
            })
            .catch(err => console.log(err))
            .finally(() => dispatch({ type: UPDATE_LOADING, loading: false }));
    }, [clients, projects]);
    const [form] = Form.useForm();
    const onFinish = (values) => {
        console.log('Received values of form: ', values);
        putClientData(values, id).then(res => {
            let newData = [...clients];
            const index = newData.findIndex((item) => id === item._id);
            if (index > -1) {
                const item = newData[index];
                newData.splice(index, 1, { ...item, ...res.data.employee });
                dispatch({ type: UPDATE_CLIENTS, clients: newData });
                dispatch({ type: UPDATE_NOTIFICATION, notification: { type: 'success', title: 'Updated client successfully' } });
                setClient({
                    ...res.data.client
                });
            }
        }).catch(error => {
            console.log('error', error);
            dispatch({
                type: UPDATE_NOTIFICATION, notification: {
                    type: 'error',
                    title: 'No such Client exist'
                }
            });
        }).finally(() => {
            form.resetFields();
            setIsEditable(false);
        });
    };
    useEffect(() => {
        // eslint-disable-next-line no-unused-expressions
        isEditable ? dispatch({ type: UPDATE_HEADER_TITLE, headerTitle: 'Edit Client Details' }) : dispatch({ type: UPDATE_HEADER_TITLE, headerTitle: 'Client Details' });
    }, [isEditable]);
    useEffect(() => {
        dispatch({ type: UPDATE_MENU_SELECTED_KEY, menuSelectedKey: '4' });
    }, []);
    return (
        <div>
            {!isEditable && <>
                <div>
                    {isSuperUser && <Button onClick={() => setIsEditable(true)}>Edit Client <EditOutlined /></Button>}
                    <Row style={{ marginTop: 20 }}>
                        <Col span={6}>
                            <h4>First Name</h4>
                            {client.firstName}
                        </Col>
                        <Col span={6}>
                            <h4>Last Name</h4>
                            {client.lastName}
                        </Col>
                        <Col span={6}>
                            <h4>Email</h4>
                            <a href={`mailto:${client.email}`} >{client.email}</a>
                        </Col>
                        <Col span={6}>
                            <h4>Contact Number</h4>
                            <a href={`tel:${client.contactNumber}`}>{client.contactNumber}</a>
                        </Col>
                    </Row>
                    <Row style={{ marginTop: 25, marginBottom: 25 }}>
                        <Col span={6}>
                            <h4>Address</h4>
                            {client.address}
                        </Col>
                    </Row>
                    <Tabs defaultActiveKey="1">
                        <TabPane tab="Projects" key="1">
                            <ClientProjectList projects={client.projects} setClient={setClient} clientId={id} />
                        </TabPane>
                    </Tabs>
                </div>
            </>}
            {
                (isEditable && isSuperUser) &&
                <Form
                    {...formItemLayout}
                    form={form}
                    name="updateClient"
                    onFinish={onFinish}
                    initialValues={{
                        firstName: client.firstName,
                        lastName: client.lastName,
                        email: client.email,
                        contactNumber: client.contactNumber,
                        address: client.address
                    }}
                    scrollToFirstError
                >
                    <Form.Item
                        name="firstName"
                        label={
                            <span>
                                First Name
                            </span>
                        }
                        rules={[
                            {
                                required: true,
                                whitespace: true,
                                message: 'Please input your first name!'
                            }
                        ]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        name="lastName"
                        label={
                            <span>
                                Last Name
                            </span>
                        }
                        rules={[
                            {
                                required: true,
                                whitespace: true,
                                message: 'Please input your Last name!'
                            }
                        ]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        name="email"
                        label={
                            <span>
                                Email
                            </span>
                        }
                        rules={[
                            {
                                required: true,
                                whitespace: false,
                                message: 'Please input your Email!'
                            }
                        ]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        name="contactNumber"
                        label="Phone Number"
                        rules={[{ required: true, message: 'Please input your contact number!' }]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        name="address"
                        label="Address"
                        rules={[{ required: true, message: 'Please input your Address!' }]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item {...tailFormItemLayout}>
                        <Button type="primary" htmlType="submit">
                            Update
                        </Button>
                        <Button onClick={() => setIsEditable(false)} style={{ marginLeft: 16 }}>
                            Discard
                        </Button>
                    </Form.Item>
                </Form>
            }
        </div>
    );
}

export default ClientDetail;
