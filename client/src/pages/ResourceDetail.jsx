/* eslint-disable max-nested-callbacks */
import React, { useState, useEffect, useMemo } from 'react';
import { useParams } from 'react-router-dom';
import { Form, Space, Select, Input, AutoComplete, Button, Row, Col, Tooltip, Tabs } from 'antd';
import { MinusCircleOutlined, PlusOutlined, EditOutlined } from '@ant-design/icons';
import { useGlobalStateContext, useGlobalDispatchContext } from '../context/globalContext';
import ResourceProjectList from '../components/ResourceProjectList';
import { getResourceData, putResourceData } from '../apiActions/ResourceAction';
import { UPDATE_HEADER_TITLE, UPDATE_NOTIFICATION, UPDATE_LOADING, UPDATE_RESOURCES, UPDATE_MENU_SELECTED_KEY } from '../constants/ActionType';
const { Option } = Select;
const { TabPane } = Tabs;
// eslint-disable-next-line react/prop-types
function ResourceDetail() {
    let { id } = useParams();
    const dispatch = useGlobalDispatchContext();
    const { employees, projects, availableType, isSuperUser } = useGlobalStateContext();
    const [isEditable, setIsEditable] = useState(false);
    const [resource, setResource] = useState({
        designation: '',
        technology: [],
        firstName: '',
        lastName: '',
        userName: '',
        email: '',
        contactNumber: '',
        address: '',
        projects: []
    });
    const formItemLayout = {
        labelCol: {
            xs: {
                span: 24
            },
            sm: {
                span: 8
            }
        },
        wrapperCol: {
            xs: {
                span: 24
            },
            sm: {
                span: 8
            }
        }
    };
    const tailFormItemLayout = {
        wrapperCol: {
            xs: {
                span: 24,
                offset: 0
            },
            sm: {
                span: 16,
                offset: 8
            }
        }
    };
    const formItemLayoutWithOutLabel = {
        wrapperCol: {
            xs: { span: 24, offset: 0 },
            sm: { span: 20, offset: 4 }
        }
    };
    useEffect(() => {
        dispatch({ type: UPDATE_LOADING, loading: true });
        getResourceData(id)
            .then(({ data }) => {
                // console.log(data);
                setResource({
                    ...data.resource,
                    technology: data.resource.technology.map(t => t.value),
                    projects: data.responsibilities.map(r => {
                        let project = projects.find(p => p._id === r.project);
                        if (project) {
                            project.responsibility = r;
                            project.role = r.role;
                            return project;
                        }
                        return null;
                    }).filter(r => r)
                });
            })
            .catch(err => console.log(err))
            .finally(() => dispatch({ type: UPDATE_LOADING, loading: false }));
    }, [employees]);
    const technologiesList = useMemo(() => {
        return availableType.technology.map((t) => ({ value: t.value, label: t.value }));
    }, [availableType]);
    const [form] = Form.useForm();
    const onFinish = (values) => {
        console.log('Received values of form: ', values);
        putResourceData(values, id).then(res => {
            let newData = [...employees];
            const index = newData.findIndex((item) => id === item._id);
            if (index > -1) {
                const item = newData[index];
                newData.splice(index, 1, { ...item, ...res.data.employee });
                dispatch({ type: UPDATE_RESOURCES, employees: newData });
                dispatch({ type: UPDATE_NOTIFICATION, notification: { type: 'success', title: 'Updated resource successfully' } });
                // console.log('res', res.data.employee);
                // console.log(res.data.employee.technology.map(t => availableType.technology.find(x => x._id === t).value));
                setResource({
                    ...res.data.employee,
                    technology: res.data.employee.technology.map(t => availableType.technology.find(x => x._id === t).value)
                });
            }
        }).catch(error => {
            console.log('error', error);
            dispatch({
                type: UPDATE_NOTIFICATION, notification: {
                    type: 'error',
                    title: 'No such Resource exist'
                }
            });
        }).finally(() => {
            form.resetFields();
            setIsEditable(false);
        });
    };
    useEffect(() => {
        dispatch({ type: UPDATE_MENU_SELECTED_KEY, menuSelectedKey: '3' });
    }, []);
    useEffect(() => {
        // eslint-disable-next-line no-unused-expressions
        isEditable ? dispatch({ type: UPDATE_HEADER_TITLE, headerTitle: 'Edit Resource Details' }) : dispatch({ type: UPDATE_HEADER_TITLE, headerTitle: 'Resource Details' });
    }, [isEditable]);
    return (
        <div>
            {!isEditable && <>
                <div>
                    {isSuperUser && <Button onClick={() => setIsEditable(true)}>Edit Resource <EditOutlined /></Button>}
                    <Row style={{ marginTop: 20 }}>
                        <Col span={6}>
                            <h4>First Name</h4>
                            {resource.firstName}
                        </Col>
                        <Col span={6}>
                            <h4>Last Name</h4>
                            {resource.lastName}
                        </Col>
                        <Col span={6}>
                            <h4>Email</h4>
                            <a href={`mailto:${resource.email}`} >{resource.email}</a>
                        </Col>
                        <Col span={6}>
                            <h4>Username</h4>
                            {resource.userName}
                        </Col>
                    </Row>
                    <Row style={{ marginTop: 25, marginBottom: 25 }}>
                        <Col span={6}>
                            <h4>Contact Number</h4>
                            {resource.contactNumber}
                        </Col>
                        <Col span={6}>
                            <h4>Address</h4>
                            {resource.address}
                        </Col>
                        <Col span={12}>
                            <h4>Technologies</h4>
                            {resource.technology.length > 0 ? resource.technology.join(', ') : <b>Not Decided</b>}
                        </Col>
                    </Row>
                    <Tabs defaultActiveKey="1">
                        <TabPane tab="Projects" key="1">
                            <ResourceProjectList projects={resource.projects} setResource={setResource} />
                        </TabPane>
                    </Tabs>
                </div>
            </>}
            {
                (isEditable && isSuperUser) &&
                <Form
                    {...formItemLayout}
                    form={form}
                    name="addResource"
                    onFinish={onFinish}
                    initialValues={{
                        firstName: resource.firstName,
                        lastName: resource.lastName,
                        email: resource.email,
                        contactNumber: resource.contactNumber,
                        address: resource.address,
                        designation: resource.designation,
                        technology: resource.technology
                    }}
                    scrollToFirstError
                >
                    <Form.Item
                        name="firstName"
                        label={
                            <span>
                                First Name
                            </span>
                        }
                        rules={[
                            {
                                required: true,
                                whitespace: true,
                                message: 'Please input your first name!'
                            }
                        ]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        name="lastName"
                        label={
                            <span>
                                Last Name
                            </span>
                        }
                        rules={[
                            {
                                required: true,
                                whitespace: true,
                                message: 'Please input your Last name!'
                            }
                        ]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        name="email"
                        label={
                            <span>
                                Email
                            </span>
                        }
                        rules={[
                            {
                                required: true,
                                whitespace: false,
                                message: 'Please input your Email!'
                            }
                        ]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        name="contactNumber"
                        label="Phone Number"
                        rules={[{ required: true, message: 'Please input your contact number!' }]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        name="address"
                        label="Address"
                        rules={[{ required: true, message: 'Please input your Address!' }]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item name="designation" label="Designation" rules={[{ required: false }]}>
                        <Select
                            placeholder="Select designation"
                        >
                            {
                                availableType.designation
                                    .map((dt, index) => dt.status ? <Option key={index} value={dt.value}>
                                        <Tooltip title={dt.description}>
                                            {dt.value}
                                        </Tooltip>
                                    </Option> : null)
                            }
                        </Select>
                    </Form.Item>
                    <Form.Item label="Technologies">
                        <Form.List name="technology" {...formItemLayoutWithOutLabel}>
                            {(fields, { add, remove }) => (
                                <>
                                    {fields.map(field => (
                                        <Space key={field.key} style={{ display: 'flex', marginBottom: 8 }} align="baseline">
                                            <Form.Item
                                                {...field}
                                                rules={[{ required: true, message: 'This is required Field' }]}
                                            >
                                                <AutoComplete
                                                    style={{ width: 200 }}
                                                    options={technologiesList}
                                                    placeholder="Select Technology"
                                                    filterOption={(inputValue, option) => {
                                                        return option.label.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1;
                                                    }}
                                                />
                                            </Form.Item>
                                            <MinusCircleOutlined onClick={() => remove(field.name)} />
                                        </Space>
                                    ))}
                                    <Form.Item>
                                        <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />}>
                                            Add Technologies
                                        </Button>
                                    </Form.Item>
                                </>
                            )}
                        </Form.List>
                    </Form.Item>
                    <Form.Item {...tailFormItemLayout}>
                        <Button type="primary" htmlType="submit">
                            Update
                        </Button>
                        <Button onClick={() => setIsEditable(false)} style={{ marginLeft: 16 }}>
                            Discard
                        </Button>
                    </Form.Item>
                </Form>
            }
        </div>
    );
}

export default ResourceDetail;
