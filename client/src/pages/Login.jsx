/* eslint-disable react/prop-types */
import React, { useState, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import { Form, Input, Button, Checkbox } from 'antd';
import { useGlobalDispatchContext, useGlobalStateContext } from '../context/globalContext';
import { UPDATE_AUTHENTICATION, UPDATE_NOTIFICATION, UPDATE_HEADER_TITLE } from '../constants/ActionType';
import { postLoginCredentials } from '../apiActions/UtilAction';
import LogoBig from '../assets/images/logoBig.png';
const layout = {
    labelCol: {
        span: 24
    },
    wrapperCol: {
        span: 24
    }
};
const tailLayout = {
    wrapperCol: {
        offset: 0,
        span: 0
    }
};
function Login(props) {
    const [form] = Form.useForm();
    const { isSuperUser } = useGlobalStateContext();
    const dispatch = useGlobalDispatchContext();
    const [redirectToReferrer, setRedirectToReferrer] = useState(false);
    const { from } = props?.location?.state || { from: { pathname: isSuperUser ? '/' : '/projects' } };
    useEffect(() => {
        dispatch({ type: UPDATE_HEADER_TITLE, headerTitle: 'Account Login' });
    }, []);
    if (redirectToReferrer) {
        return <Redirect to={from} />;
    }
    const onLoginSuccess = (isSuper) => {
        dispatch({ type: UPDATE_AUTHENTICATION, load: true, isSuperUser: isSuper });
        setRedirectToReferrer(true);
    };
    const onFinish = (values) => {
        postLoginCredentials({ user: values }).then(res => {
            if (res.data.token) {
                localStorage.setItem('token', res.data.token);
                localStorage.setItem('isSuperUser', res.data.isSuperUser);
                onLoginSuccess(res.data.isSuperUser);
            }
        }).catch(error => {
            dispatch({
                type: UPDATE_NOTIFICATION, notification: {
                    type: 'error',
                    title: error.response.data ? error.response.data.error : 'Login failed!'
                }
            });
            console.log(error);
        }).finally(() => form.resetFields());

    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };
    return (
        <div style={{ background: '#001529', minHeight: '100vh', width: '100vw', display: 'flex', alignItems: 'center' }}>
            <div style={{ width: '70%', margin: 'auto', padding: '5vw' }} >
                <img src={LogoBig} alt="doez logo" style={{ height: '10vh' }} />
                <h1 style={{ marginTop: 50, color: 'white' }}>Project & Resource management tool</h1>
                <p style={{ color: 'white' }}>Ease your task to manage resources and clients in your project.</p>
            </div>
            <div style={{ width: '30%', background: '#fff' }} >
                <div style={{
                    display: 'flex',
                    flexDirection: 'column',
                    justifyContent: 'center',
                    alignItems: 'center',
                    height: '100vh'
                }}>
                    <h1 style={{ marginBottom: 50, fontSize: 50, fontWeight: 'bolder' }}>Login</h1>
                    <Form
                        {...layout}
                        form={form}
                        name="basic"
                        initialValues={{
                            isSuperUser: false
                        }}
                        onFinish={onFinish}
                        onFinishFailed={onFinishFailed}
                    >
                        <Form.Item
                            // label="Username"
                            name="username"
                            rules={[
                                {
                                    required: true,
                                    message: 'Please input your username!'
                                }
                            ]}
                        >
                            <Input placeholder='Username' />
                        </Form.Item>

                        <Form.Item
                            // label="Password"
                            name="password"
                            rules={[
                                {
                                    required: true,
                                    message: 'Please input your password!'
                                }
                            ]}
                        >
                            <Input.Password placeholder='Password' />
                        </Form.Item>
                        <Form.Item {...tailLayout} name="isSuperUser" valuePropName="checked">
                            <Checkbox>Is Super User?</Checkbox>
                        </Form.Item>
                        <Form.Item {...tailLayout}>
                            <Button type="primary" htmlType="submit">
                                Submit
                            </Button>
                        </Form.Item>
                    </Form>
                </div>
            </div>
        </div >
    );
}

export default Login;
