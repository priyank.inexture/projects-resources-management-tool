/* eslint-disable max-nested-callbacks */
import React, { useEffect, useState, useMemo } from 'react';
import { MinusCircleOutlined, PlusOutlined, EditOutlined } from '@ant-design/icons';
import { Form, Space, Select, Input, InputNumber, AutoComplete, Button, Row, Col, Tooltip, Tabs } from 'antd';
import { useParams } from 'react-router-dom';
import { currencyFormat } from '../utils/CommonUtils';
import { putProjectData, getProjectInfo } from '../apiActions/ProjectAction';
import { useGlobalDispatchContext, useGlobalStateContext } from '../context/globalContext';
import { UPDATE_LOADING, UPDATE_NOTIFICATION, UPDATE_PROJECTS, UPDATE_HEADER_TITLE, UPDATE_MENU_SELECTED_KEY } from '../constants/ActionType';
import ClientList from '../components/ClientList';
import ResourceList from '../components/ResourceList';
const { Option } = Select;
const { TabPane } = Tabs;
// eslint-disable-next-line react/prop-types
function ProjectDetail() {
    // console.log('isEdit', isEdit);
    const dispatch = useGlobalDispatchContext();
    const { projects, currency, isSuperUser } = useGlobalStateContext();
    let { id } = useParams();

    const [isEditable, setIsEditable] = useState(false);
    const [project, setProject] = useState({
        allocation: '',
        amount: 0,
        clientList: [],
        name: '',
        paymentType: '',
        priority: '',
        reviewer: {
            address: '',
            contactNumber: '',
            email: '',
            firstName: '',
            lastName: '',
            userName: ''
        },
        status: '',
        teamLead: {
            address: '',
            contactNumber: '',
            email: '',
            firstName: '',
            lastName: '',
            userName: ''
        },
        _id: '',
        tech: [],
        responsibilities: []
    });
    useEffect(() => {
        dispatch({ type: UPDATE_LOADING, loading: true });
        getProjectInfo(id)
            .then(({ data }) => {
                // console.log(data);
                setProject({
                    ...data.project,
                    // eslint-disable-next-line max-nested-callbacks
                    tech: data.project.tech.map(t => t.value),
                    responsibilities: data.responsibilities
                });
            })
            .catch(err => console.log(err))
            .finally(() => dispatch({ type: UPDATE_LOADING, loading: false }));
    }, [projects]);
    const { availableType, employees } = useGlobalStateContext();
    const formItemLayout = {
        labelCol: {
            xs: {
                span: 24
            },
            sm: {
                span: 8
            }
        },
        wrapperCol: {
            xs: {
                span: 24
            },
            sm: {
                span: 16
            }
        }
    };
    const tailFormItemLayout = {
        wrapperCol: {
            xs: {
                span: 24,
                offset: 0
            },
            sm: {
                span: 16,
                offset: 8
            }
        }
    };
    const [form] = Form.useForm();
    // const [autoCompleteResult, setAutoCompleteResult] = useState([]);
    const formItemLayoutWithOutLabel = {
        wrapperCol: {
            xs: { span: 24, offset: 0 },
            sm: { span: 20, offset: 4 }
        }
    };
    const onFinish = (values) => {
        values.technologies = [...new Set(values.technologies)];
        // console.log('Updated ', values);
        // // console.log('Received values of form: ', values);
        putProjectData(values, id).then(res => {
            dispatch({ type: UPDATE_PROJECTS, projects: res.data.projects });
            dispatch({
                type: UPDATE_NOTIFICATION, notification: {
                    type: 'success',
                    title: 'Updated Project Info successfully'
                }
            });
            console.log(res.data);
            setIsEditable(false);
        }).catch(error => {
            if (error.response) {
                dispatch({
                    type: UPDATE_NOTIFICATION, notification: {
                        type: 'error',
                        title: error.response.data.error ? error.response.data.error : 'Some error Occured, cannot perform action!'
                    }
                });
            }
        });
    };
    useEffect(() => {
        if (isEditable) {
            form.resetFields();
        }
        // console.log(project);
    }, [project]);
    // const employeesList = useMemo(() => {
    //     return employees.map((e) => ({ value: e.userName, label: e.userName }));
    // }, [employees]);
    const technologiesList = useMemo(() => {
        return availableType.technology.map((t) => (t.status ? { value: t.value, label: t.value } : null)).filter(t => t);
    }, [availableType]);
    const options = useMemo(() => {
        let opt = availableType.designation.map(d => ({ label: d.value, options: employees.filter(e => e.designation === d.value).map(emp => ({ value: emp.userName, label: `${emp.firstName} ${emp.lastName}` })) }));
        return opt.filter(o => o.options.length !== 0);
        // return employees.map((e) => ({ value: e.userName, label: e.userName }));
    }, [employees, availableType]);
    useEffect(() => {
        // eslint-disable-next-line no-unused-expressions
        isEditable ? dispatch({ type: UPDATE_HEADER_TITLE, headerTitle: 'Edit Project Details' }) : dispatch({ type: UPDATE_HEADER_TITLE, headerTitle: 'Project Details' });
    }, [isEditable]);
    useEffect(() => {
        dispatch({ type: UPDATE_MENU_SELECTED_KEY, menuSelectedKey: '2' });
    }, []);
    return (
        <div>
            {!isEditable && <>
                <div>
                    {isSuperUser && <Button onClick={() => setIsEditable(true)}>Edit Project <EditOutlined /></Button>}
                    <Row style={{ marginTop: 20 }}>
                        <Col span={6}>
                            <h4>Project Name</h4>
                            {project.name}
                        </Col>
                        <Col span={6}>
                            <h4>Payment Type</h4>
                            {project.paymentType}
                        </Col>
                        <Col span={6}>
                            <h4>Amount</h4>
                            {currencyFormat(project.amount, currency.to, currency.toRate)}
                        </Col>
                        <Col span={6}>
                            <h4>Allocation</h4>
                            {project.allocation}
                        </Col>
                    </Row>
                    <Row style={{ marginTop: 25, marginBottom: 25 }}>
                        <Col span={6}>
                            <h4>Priority</h4>
                            {project.priority}
                        </Col>
                        <Col span={6}>
                            <h4>Status</h4>
                            {project.status}
                        </Col>
                        <Col span={12}>
                            <h4>Technologies</h4>
                            {project.tech.length > 0 ? project.tech.join(', ') : <b>Not Decided</b>}
                        </Col>
                    </Row>
                    <Row style={{ marginTop: 25, marginBottom: 25 }}>
                        <Col span={6}>
                            <h4>Reviewer</h4>
                            {project.reviewer ? `${project.reviewer.firstName} ${project.reviewer.lastName}` : <b>Not Assigned</b>}
                        </Col>
                        <Col span={6}>
                            <h4>Team Lead</h4>
                            {project.teamLead ? `${project.teamLead.firstName} ${project.teamLead.lastName}` : <b>Not Assigned</b>}
                        </Col>
                    </Row>
                    <Tabs defaultActiveKey="1">
                        <TabPane tab="Resources" key="1">
                            <ResourceList list={project.responsibilities} setProject={setProject} />
                        </TabPane>
                        <TabPane tab="Clients" key="2">
                            <ClientList list={project.clientList} />
                        </TabPane>
                    </Tabs>
                </div>
            </>}
            {
                (isEditable && isSuperUser) && <>
                    <Form
                        {...formItemLayout}
                        form={form}
                        name="update"
                        onFinish={onFinish}
                        initialValues={{
                            name: project.name,
                            amount: project.amount,
                            paymentType: project.paymentType,
                            allocation: project.allocation,
                            priority: project.priority,
                            status: project.status,
                            teamLead: project.teamLead ? project.teamLead.userName : '',
                            reviewer: project.reviewer ? project.reviewer.userName : '',
                            technologies: project.tech
                        }}
                        scrollToFirstError
                    >
                        <Form.Item
                            name="name"
                            label={
                                <span>
                                    Project Name
                                </span>
                            }
                            rules={[
                                {
                                    required: true,
                                    message: 'Please input your Project Name!',
                                    whitespace: true
                                }
                            ]}
                        >
                            <Input />
                        </Form.Item>
                        <Form.Item name="paymentType" label="Payment Type" rules={[{ required: true }]}>
                            <Select
                                placeholder="Payment Type"
                            >
                                {
                                    availableType.payment
                                        .map((pt, index) => pt.status ? <Option key={index} value={pt.value}>
                                            <Tooltip title={pt.description}>
                                                {pt.value}
                                            </Tooltip>
                                        </Option> : null)
                                }
                            </Select>
                        </Form.Item>
                        <Form.Item name="amount" label="Total Amount (in USD)" rules={[{ required: true }]}>
                            <InputNumber
                                min={0}
                                style={{ width: 150 }}
                                max={100000000}
                            />
                        </Form.Item>
                        <Form.Item name="allocation" label="Allocation" rules={[{ required: true }]}>
                            <Select
                                placeholder="Select Allocation Type"
                            >
                                {
                                    availableType.allocation
                                        .map((at, index) => at.status ? <Option key={index} value={at.value}>
                                            <Tooltip title={at.description}>
                                                {at.value}
                                            </Tooltip>
                                        </Option> : null)
                                }
                            </Select>
                        </Form.Item>
                        <Form.Item name="priority" label="Priority" rules={[{ required: true }]}>
                            <Select
                                placeholder="Select Priority"
                            >
                                {
                                    availableType.priority
                                        .map((pt, index) => pt.status ? <Option key={index} value={pt.value}>
                                            <Tooltip title={pt.description}>
                                                {pt.value}
                                            </Tooltip>
                                        </Option> : null)
                                }
                            </Select>
                        </Form.Item>
                        <Form.Item name="status" label="Status" rules={[{ required: true }]}>
                            <Select
                                placeholder="Select status"
                            >
                                {
                                    availableType.status
                                        .map((st, index) => st.status ? <Option key={index} value={st.value}>
                                            <Tooltip title={st.description}>
                                                {st.value}
                                            </Tooltip>
                                        </Option> : null)
                                }
                            </Select>
                        </Form.Item>
                        <Form.Item name="teamLead" label="Team Lead" rules={[{ required: false }]}>
                            <AutoComplete
                                style={{ width: 200 }}
                                options={options}
                                placeholder="Select Team Lead"
                                filterOption={(inputValue, option) => {
                                    return option.label.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1;
                                }}
                            />
                        </Form.Item>
                        <Form.Item name="reviewer" label="Reviewer" rules={[{ required: false }]}>
                            <AutoComplete
                                style={{ width: 200 }}
                                options={options}
                                placeholder="Select Reviewer"
                                filterOption={(inputValue, option) => {
                                    return option.label.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1;
                                }}
                            />
                        </Form.Item>
                        <Form.Item label="Technologies">
                            <Form.List name="technologies" {...formItemLayoutWithOutLabel}>
                                {(fields, { add, remove }) => (
                                    <>
                                        {fields.map(field => (
                                            <Space key={field.key} style={{ display: 'flex', marginBottom: 8 }} align="baseline">
                                                <Form.Item
                                                    {...field}
                                                    rules={[{ required: true, message: 'This is required Field' }]}
                                                >
                                                    <AutoComplete
                                                        style={{ width: 200 }}
                                                        options={technologiesList}
                                                        placeholder="Select Technology"
                                                        filterOption={(inputValue, option) => {
                                                            return option.label.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1;
                                                        }}
                                                    />
                                                </Form.Item>
                                                <MinusCircleOutlined onClick={() => remove(field.name)} />
                                            </Space>
                                        ))}
                                        <Form.Item>
                                            <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />}>
                                                Add Technologies
                                            </Button>
                                        </Form.Item>
                                    </>
                                )}
                            </Form.List>
                        </Form.Item>
                        <Form.Item {...tailFormItemLayout}>
                            <Button type="primary" htmlType="submit">
                                Update
                            </Button>
                            <Button onClick={() => setIsEditable(false)} style={{ marginLeft: 16 }}>
                                Discard
                            </Button>
                        </Form.Item>
                    </Form>
                </>
            }
        </div>
    );
}

export default ProjectDetail;
