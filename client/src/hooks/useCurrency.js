// eslint-disable-next-line no-unused-vars
import React,{ useEffect , useState } from 'react';
import { useGlobalDispatchContext } from '../context/globalContext';
import { UPDATE_CURRENCY } from '../constants/ActionType';
// eslint-disable-next-line no-unused-vars
import { fetchCurrencyRates } from '../apiActions/UtilAction';
function useCurrency() {
    // eslint-disable-next-line no-unused-vars
    const dispatch = useGlobalDispatchContext();
    // eslint-disable-next-line no-unused-vars
    const [state, setState] = useState({ base: 'USD',rates: {
        // eslint-disable-next-line max-len
        'CAD': 1.2617323908,'HKD': 7.7747210629,'ISK': 126.4798569117,'PHP': 48.5605996082,'DKK': 6.3341282685,'HUF': 309.4284984243,'CZK': 22.2485307895,'GBP': 0.7271782642,'RON': 4.1912954604,'SEK': 8.7277914999,'IDR': 14533.0040030662,'INR': 73.4639298186,'BRL': 5.7648411549,'RUB': 75.9382505749,'HRK': 6.4473213525,'JPY': 110.2802146325,'THB': 31.2699088664,'CHF': 0.9417426114,'EUR': 0.8517162082,'MYR': 4.1510092837,'BGN': 1.6657865599,'TRY': 8.3297845158,'CNY': 6.5713312324,'NOK': 8.5693722852,'NZD': 1.4303722,'ZAR': 14.9387616046,'USD': 1.0,'MXN': 20.6338472021,'SGD': 1.3469891832,'AUD': 1.3132612214,'ILS': 3.3330210374,'KRW': 1133.9323737331,'PLN': 3.9674644408
    },
    date: '2021-03-30'
    });
    useEffect(() => {
        // console.log('Updating rates');
        // fetchCurrencyRates().then(res => {
        //     console.log(res);
        //     setState(res.data);
        //     dispatch({ type: UPDATE_CURRENCY, currency: res.data });
        //     console.log(`Using rates of ${res.data.date}`);
        // });
        dispatch({ type: UPDATE_CURRENCY, currency: state });
    }, []);
    return state;
}

export default useCurrency;
