Front end for Projects & Resources Management Tool build with react.

# About dependencies

## API used

[`exchangeratesapi`](https://exchangeratesapi.io/)
Gives json for rates of currency

## Dependencies

[`@ant-design/icons`](https://www.npmjs.com/package/@ant-design/icons)
Ant Design Icons for React

[`antd`](https://www.npmjs.com/package/antd)
An enterprise-class UI design language and React UI library.

[`axios`](https://www.npmjs.com/package/axios)
Promise based HTTP client for the browser and node.js

[`prop-types`](https://www.npmjs.com/package/prop-types)
for runtime type checking for React props and similar objects.

[`react`](https://www.npmjs.com/package/react) & [`react-dom`](https://www.npmjs.com/package/react-dom) for react components

For polyfilling, We will use [`corejs`](https://www.npmjs.com/package/core-js) Since [`@babel/polyfill`](https://babeljs.io/docs/en/babel-polyfill/) has been deprecated.

## devDependencies for webpack, eslint, babel

### **Babel**

[`@babel/core`](https://babeljs.io/docs/en/babel-core/) has core dependencies for Babel

[`babel-loader`](https://webpack.js.org/loaders/babel-loader/) This package allows transpiling JavaScript files using Babel and webpack

[`@babel/preset-env`](https://babeljs.io/docs/en/next/babel-preset-env.html) With this you don’t have to specify if you will be writing ES2015, ES2016 or ES2017. Babel will automatically detect and transpile accordingly.

[`@babel/preset-react`](https://babeljs.io/docs/en/babel-preset-react/) Tells Babel we will be using React

[`@babel/plugin-syntax-dynamic-import`](https://babeljs.io/docs/en/babel-plugin-syntax-dynamic-import) used for dynamic imports

[`@babel/plugin-proposal-class-properties `](https://babeljs.io/docs/en/babel-plugin-proposal-class-properties) to use class properties.

[`react-hot-loader/babel`](https://github.com/gaearon/react-hot-loader) is used in Hot Module Replacement

### **Webpack**

[`webpack`](https://webpack.js.org/) Module bundler

[`webpack-cli`](https://github.com/webpack/webpack-cli) Command Line Interface, needed for Webpack

[`webpack-dev-server`](https://webpack.js.org/configuration/dev-server/) Provides a development server for your application

[`webpack-bundle-analyzer`](https://github.com/webpack-contrib/webpack-bundle-analyzer) Helps to visiualize bundle size and is usefull for optimization

[`webpack-merge`](https://github.com/survivejs/webpack-merge) used to merge comman config with dev or production as per requirement.

[`rimraf`](https://www.npmjs.com/package/rimraf) used to delete dist folder when build command is executed.

### **Eslint**

[`eslint`](https://eslint.org/docs/user-guide/getting-started) use to create [`.eslintrc`](./.eslintrc.json) and linting purpose.

[`eslint-config-react`](https://www.npmjs.com/package/eslint-config-react) as we are using react project.

# Flow

## [`Webpack`](https://webpack.js.org/concepts/)

webpack is a tools which manages all the assets throught

[`webapack.config.js`](./webpack.config.js), Used to merge common and dev or prod configuration.

[`webpack.common.js`](./build-utils/webpack.common.js) has all the configuration comman in both producation and developlment

[`webpack.dev.js`](./build-utils/webpack.dev.js) has configuration for developlment

[`webpack.prod.js`](./build-utils/webpack.prod.js) has all the configuration comman in both producation and developlment

### Plugins

- [`HtmlWebpackPlugin`](https://webpack.js.org/plugins/html-webpack-plugin/) used for creation of HTML files to serve your webpack bundles.

- [`HotModuleReplacementPlugin`](https://webpack.js.org/plugins/hot-module-replacement-plugin/) enables Hot Module Replacement, otherwise known as HMR.

- [`MiniCssExtractPlugin`](https://webpack.js.org/plugins/mini-css-extract-plugin/) extracts CSS into separate files. It creates a CSS file per JS file which contains CSS. It supports On-Demand-Loading of CSS and SourceMaps.

- [`BundleAnalyzerPlugin`](https://github.com/webpack-contrib/webpack-bundle-analyzer) use to visiualize bundle size.

### Loaders

- [`style-loader`](https://webpack.js.org/loaders/style-loader) Add exports of a module as style to DOM
- [`css-loader`](https://webpack.js.org/loaders/css-loader) Loads CSS file with resolved imports and returns CSS code
- [`sass-loader`](https://webpack.js.org/loaders/sass-loader) Loads and compiles a SASS/SCSS file
- [`postcss-loader`](https://webpack.js.org/loaders/postcss-loader) Loads and transforms a CSS/SSS file using PostCSS

- [`file-loader`](https://webpack.js.org/loaders/file-loader) Emits the file into the output folder and returns the (relative) URL

## [`Babel`](https://babeljs.io/)

Babel is a toolchain that is mainly used to convert ECMAScript 2015+ code into a backwards compatible version of JavaScript in current and older browsers or environments.

Its configuration are managed through [`.babelrc`](./.babelrc)

### Plugins

- [`@babel/plugin-syntax-dynamic-import`](https://babeljs.io/docs/en/babel-plugin-syntax-dynamic-import) used for dynamic imports
- [`@babel/plugin-proposal-class-properties `](https://babeljs.io/docs/en/babel-plugin-proposal-class-properties) to use class properties.
- [`react-hot-loader/babel`](https://github.com/gaearon/react-hot-loader) is used in Hot Module Replacement

# References

- [`freecodecamp`](https://www.freecodecamp.org/news/learn-webpack-for-react-a36d4cac5060/)
- [`Webpack`](https://webpack.js.org/concepts/)
- [`Babel`](https://babeljs.io/)
