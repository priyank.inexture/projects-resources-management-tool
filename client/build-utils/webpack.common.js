const commonPaths = require('./common-paths');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const target = process.env.NODE_ENV === 'production' ? 'browserslist' : 'web';
const config = {
    output: {
        path: commonPaths.outputPath,
        publicPath: '/'
    },
    target: target,
    module: {
        rules: [
            {
                test: /\.less$/,
                use: [{
                    loader: 'style-loader'
                }, {
                    loader: 'css-loader' // translates CSS into CommonJS
                }, {
                    loader: 'less-loader', // compiles Less to CSS
                    options: {
                        lessOptions: { // If you are using less-loader@5 please spread the lessOptions to options directly
                            modifyVars: {
                                //  'primary-color': '#1DA57A',
                                //  'link-color': '#1DA57A',
                                //  'border-radius-base': '2px',
                            },
                            javascriptEnabled: true
                        }
                    }
                }]
            // ...other rules
            },
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                resolve: {
                    extensions: ['.js', '.jsx']
                },
                use: ['babel-loader']
            }
            // {
            //     test: /\.(png|j?g|svg|gif)?$/,
            //     use: 'file-loader'
            // }
        ]
    },
    optimization: {
        splitChunks: {
            cacheGroups: {
                styles: {
                    name: 'styles',
                    test: /\.css$/,
                    chunks: 'all',
                    enforce: true
                },
                vendor: {
                    test: /[\\/]node_modules[\\/]/,
                    name: 'vendor',
                    chunks: 'all',
                    enforce: true
                }
            }
        }
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './public/index.html',
            favicon: './public/favicon.ico'
        })
    ]
};
module.exports = config;
