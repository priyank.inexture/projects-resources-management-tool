const commonPaths = require('./common-paths');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const config = {
    mode: 'production',
    entry: {
        app: [`${commonPaths.appEntry}/index.jsx`]
    },
    output: {
        filename: 'static/[name].[hash].js',
        path: commonPaths.outputPath,
        publicPath: './'
    },
    devtool: 'source-map',
    module: {
        rules: [
            {
                test: /\.(scss|css)$/,
                // exclude: /node_modules/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            esModule: true,
                            importLoaders: 2,
                            sourceMap: true,
                            modules: {
                                exportLocalsConvention: 'camelCase'
                            }
                        }
                    },
                    {
                        loader: 'postcss-loader'
                    },
                    { loader: 'sass-loader' }
                ]
            },
            {
                test: /\.(png|j?g|svg|gif)?$/,
                loader: 'file-loader',
                options: {
                    publicPath: './resource-managment-app/images',
                    outputPath: 'images',
                    name: '[name].[ext]'
                }
            }
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: 'styles/[name].[chunkhash].css'
        })
    ],
    optimization: {
        minimizer: [new UglifyJsPlugin()]
    }
};
module.exports = config;
