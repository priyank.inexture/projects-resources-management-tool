This is tool build with Express and React with antd as UI framework.

# Demo videos

## Project

- [`Add a project and listing`](/demos/project-add-list.mkv)
- [`Updating project details`](/demos/project-update-detail.mkv)
- [`Deleting a project from project list`](/demos/project-delete.mkv)
- [`Adding and deleting Client from a project`](/demos/project-add-clients.mkv)

## Client

- [`Client list`](/demos/client-listing.mkv)
- [`Adding new client`](/demos/client-add.mkv)
- [`Updating existing client`](/demos/client-update.mkv)
- [`Deleting existing client`](/demos/client-delete.mkv)

## Resources

- [`Resource list`](/demos/resource-listing.mkv)
- [`Adding new resource`](/demos/resource-add.mkv)
- [`Updating existing resource`](/demos/resource-update.mkv)
- [`Deleting existing resource`](/demos/resource-delete.mkv)

## features

- [`Changing currency formatting`](/demos/other-currency.mkv)

# Run project in development mode

- make database at mongodb://localhost:27017/resourceManagerDB
- or change db url in [`index.js`](./api/index.js) line number 13.
- It will atomatically insert dummy data for testing purpose once server is started.

### To run server

```
projects-resources-management-tool/api $ npm start
```

### To run React App in dev

```
projects-resources-management-tool/client $ yarn dev
```
